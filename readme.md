# README #

gameseed, prototype video game console

### What is this repository for? ###

This project is an open-source video game console inspired by projects such as [Uzebox](http://belogic.com/uzebox/index.asp) and [Pico-8](https://www.lexaloffle.com/pico-8.php),
along with inspiration from the Nintendo Entertainment system and the SEGA Genesis. I have always had a passion about learning about the 8-bit computers, arcade machines, and video game consoles.
This is my ode to that time!

Most of the work here is old, but it will be revamped once I send off the prototype pcbs to be manufactured.
I plan to include a status report and blog with my design thoughts as I go through the process of creation on the [wiki](https://bitbucket.org/beefok/gameseed/wiki/Home).

The schematics and pcb were developed in Altium's circuitmaker, 

### Who do I talk to? ###

* the designer is beefok, electrodev@gmail.com