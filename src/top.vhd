library ieee;
use ieee.std_logic_1164.all;
use work.cpu_pkg.all;

entity top is port
(  rs, ck : in  std_logic;
   mw     : out std_logic
);
end top;

architecture rtl of top is

   -- cpu design
   component cpu_top is port
   (  rs, ck : in  std_logic;
      mb     : out type_mb;
      ma     : out bits;
      mi     : in  bits;
      mo     : out bits
   );
   end component;
   
   -- sys controller
   component sys_top is port
   (  rs, ck : in  std_logic;
   -- irq interface
      iq     : in  std_logic_vector(3 downto 0);
   -- button interface
      bi     : in  std_logic_vector(7 downto 0)
   -- i/o register interface
      mw, mr : in  std_logic;
      mi     : in  bits;
      mo     : out bits;
      ir     : out std_logic;
   );
   end component;
   
   -- dma controller
   component dma_top is port
   (  rs, ck : in  std_logic;
   -- i/o register interface
      mw     : in  std_logic;
      mi     : in  bits;
      ir     : out std_logic;
   -- controller interface
      pr, pw : out std_logic;
      pi     : in  bits;
      po     : out bits;
      pa     : out phya
   );
   end component;
   
   -- cpu block memory
   component cpu_mem is port
   (  ck, rs : in  std_logic;
   -- cpu interface
      mw     : in  std_logic;
      ma, mo : in  bits;
      mi     : out bits;
   -- dma controller interface
      pw     : in  std_logic;
      pa     : in  phya;
      pi     : in  bits;
      po     : out bits
   );
   end component;
   
   -- gpu object attribute memory
   component gpu_oam is port
   (  ck, rs         : in  std_logic;
      md             : in  std_logic;
   -- gpu interface
      aw, bw, cw, dw : in  std_logic;
      aa, ba, ca, da : in  std_logic_vector( 9 downto 0);
      ai, bi, ci, di : in  std_logic_vector( 8 downto 0);
      ao, bo, co, do : out std_logic_vector( 8 downto 0);
   -- dma controller interface
      pw, pr         : in  std_logic;
      pa             : in  phya;
      pi             : in  bits;
      po             : out bits
   );
   end component;
   
   -- gpu character memory
   component gpu_chr is port
   (  ck, rs         : in  std_logic;
      md             : in  std_logic;
   -- gpu interface
      mw             : in  std_logic;
      ma             : in  std_logic_vector(10 downto 0);
      mi             : in  std_logic_vector( 6 downto 0);
      mo             : out std_logic_vector( 6 downto 0);
   -- dma controller interface
      pw, pr         : in  std_logic;
      pa             : in  phya;
      pi             : in  bits;
      po             : out bits
   );
   end component;
   
   -- spu effects memory
   component spu_mem is port
   (  ck, rs : in  std_logic;
   -- spu interface
      mw     : in  std_logic;
      ma, mi : in  bits;
      mo     : out bits;
   -- dma controller interface
      pw     : in  std_logic;
      pa     : in  phya;
      pi     : in  bits;
      po     : out bits
   );
   end component;
   
   signal cpu_ma, cpu_mi, cpu_mo : bits;
   signal cpu_mb : type_mb;
-- signal mw : std_logic;

   -- interrupt requests
   signal gpu_ir, spu_ir, dma_ir : std_logic;
   
   signal cpu_ir : std_logic;
   
   -- i/o port address control
   signal sys_mw, gpu_mw, spu_mw, dma_mw : std_logic;
   signal sys_mr, gpu_mr, spu_mr, dma_mr : std_logic;
   
   -- cpu ram memory write control
   signal cpu_mw : std_logic;
   
   -- physical address memory mapping control signals
   signal pw, pw_cpu_ram, pw_gpu_oam, pw_gpu_chr, pw_spu_ram, pw_spi_ram, pw_spi_rom : std_logic;
   signal pr, pr_cpu_ram, pr_gpu_oam, pr_gpu_chr, pr_spu_ram, pr_spi_ram, pr_spi_rom : std_logic;

begin

   mw <= dma_mw;

   -- central processing unit, cpu
	u0: cpu_top port map
   (  ck => ck, rs => rs, ir => cpu_ir,
      mb => cpu_mb, ma => cpu_ma, mo => cpu_mo, mi => cpu_mi
   );
   
   -- interrupt requests
   ir <= '0' & gpu_ir & spu_ir & dma_ir;
   
   -- system controller
   u1: sys_top port map
   (  ck => ck, rs => rs,
   -- irq interface
      iq => ir,
   -- button interface
      bi => bi,
   -- cpu interface
      mw => cpu_bw, mr => cpu_br,
      mi => cpu_mo, mo => cpu_mi,
      ir => cpu_ir
   );
   end component;
   
   -- i/o port address control
   sys_mw <= '1' when mb = mb_bw and cpu_ma(1 downto 0) = "00" else '0';
   gpu_mw <= '1' when mb = mb_bw and cpu_ma(1 downto 0) = "01" else '0';
   spu_mw <= '1' when mb = mb_bw and cpu_ma(1 downto 0) = "10" else '0';
   dma_mw <= '1' when mb = mb_bw and cpu_ma(1 downto 0) = "11" else '0';
   sys_mr <= '1' when mb = mb_br and cpu_ma(1 downto 0) = "00" else '0';
   gpu_mr <= '1' when mb = mb_br and cpu_ma(1 downto 0) = "01" else '0';
   spu_mr <= '1' when mb = mb_br and cpu_ma(1 downto 0) = "10" else '0';
   dma_mr <= '1' when mb = mb_br and cpu_ma(1 downto 0) = "11" else '0';
   
   cpu_mw <= '1' when mb = mb_dw else '0';
   cpu_br <= '1' when mb = mb_br else '0';
   cpu_bw <= '1' when mb = mb_bw else '0';

   -- direct memory access controller, dma
   u1: dma_top port map
   (  ck => ck, rs => rs,
   -- cpu interface
      ir => dma_irq, mw => cpu_mw, mi => cpu_mo,
   -- dma interface
      pr => pr, pw => pw, pi => pi, po => po, pa => pa
   );
   
   -- graphics processing unit, gpu
   -- u2: gpu_top port map
   -- (  ck => ck, rs => rs,
   -- -- cpu interface
      -- ir => gpu_irq, mw => cpu_mw, mi => cpu_mo
   -- );
   
   -- sound processing unit, spu
   -- u3: spu_top port map
   -- (  ck => ck, rs => rs,
   -- -- cpu interface
      -- ir => spu_irq, mw => cpu_mw, mi => cpu_mo
   -- );
   
   -- physical address memory mapping
   pw_cpu_ram <= '1' when pw <= '1' and pa(18 downto 13) = o"00" else '0'; -- cpu ram   4k, 000000 - 007777
   pw_gpu_oam <= '1' when pw <= '1' and pa(18 downto 13) = o"01" else '0'; -- gpu oam   4k, 010000 - 017777
   pw_gpu_chr <= '1' when pw <= '1' and pa(18 downto 13) = o"02" else '0'; -- gpu chr   4k, 020000 - 027777
   pw_spu_ram <= '1' when pw <= '1' and pa(18 downto 13) = o"03" else '0'; -- spu ram   4k, 030000 - 037777
   pw_spi_ram <= '1' when pw <= '1' and pa(18 downto 17) =  "01" else '0'; -- spi ram  64k, 200000 - 377777
   pw_spi_rom <= '1' when pw <= '1' and pa(18          ) =   "1" else '0'; -- spi rom 128k, 400000 - 777777
   pr_cpu_ram <= '1' when pr <= '1' and pa(18 downto 13) = o"00" else '0'; -- cpu ram   4k, 000000 - 007777
   pr_gpu_oam <= '1' when pr <= '1' and pa(18 downto 13) = o"01" else '0'; -- gpu oam   4k, 010000 - 017777
   pr_gpu_chr <= '1' when pr <= '1' and pa(18 downto 13) = o"02" else '0'; -- gpu chr   4k, 020000 - 027777
   pr_spu_ram <= '1' when pr <= '1' and pa(18 downto 13) = o"03" else '0'; -- spu ram   4k, 030000 - 037777
   pr_spi_ram <= '1' when pr <= '1' and pa(18 downto 17) =  "01" else '0'; -- spi ram  64k, 200000 - 377777
   pr_spi_rom <= '1' when pr <= '1' and pa(18          ) =   "1" else '0'; -- spi rom 128k, 400000 - 777777

   -- cpu memory
   m0: cpu_mem port map
   (  ck => ck,
   -- cpu interface
      mw => cpu_mw, ma => cpu_ma, mo => cpu_mo, mi => cpu_mi,
   -- dma interface
      pw => pw_cpu_ram, pi => po, po => po_cpu, pa => pa
   );
   
   -- gpu object attribute memory
   m1: gpu_oam port map
   (  ck => ck, rs => rs, md => gpu_md,
   -- gpu interface
      aw => gpu_aw, aa => gpu_aa, ai => gpu_ai, ao => gpu_ao,
      bw => gpu_bw, ba => gpu_ba, bi => gpu_bi, bo => gpu_bo,
      cw => gpu_cw, ca => gpu_ca, ci => gpu_ci, co => gpu_co,
      dw => gpu_dw, da => gpu_da, di => gpu_di, do => gpu_do,
   -- dma interface
      pr => pr, pw => pw, pi => pi, po => po, pa => pa
   );
   
   -- gpu character memory
   m2: gpu_chr port map
   (  ck => ck, rs => rs, md => gpu_md,
   -- gpu interface
      mw => gpu_mw, ma => gpu_ma, mi => gpu_mi, mo => gpu_mo,
   -- dma interface
      pr => pr, pw => pw, pi => pi, po => po, pa => pa
   );
   
   -- spu effects memory
   m3: spu_mem port map
   (  ck => ck, rs => rs,
   -- spu interface
      mw => spu_mw, ma => spu_ma, mi => spu_mi, mo => spu_mo,
   -- dma interface
      pr => pr, pw => pw, pi => pi, po => po, pa => pa
   );

end rtl;