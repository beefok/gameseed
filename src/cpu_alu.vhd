library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.cpu_pkg.all;

entity cpu_alu is port
(  cq         : in  type_cq;
   ea, eb     : in  bits;
   sk         : out std_logic;
   ex         : out bits
);
end cpu_alu;

architecture rtl of cpu_alu is

	-- sh: 0 is 1 step, 1 is 8 total steps, 2 is 12 steps
	constant sh : integer := 0;
	
	signal eq, lr, gr : boolean;
	signal eadd, a, b : signed(11 downto 0);
	signal sn, zn : std_logic_vector(11 downto 0);
   signal sb : std_logic_vector( 3 downto 0);
	signal eror, erol, eshl, eshr, emov, eand, eorr, exor : std_logic_vector(11 downto 0);

begin

   sn <= (others => ea(0));
   zn <= (others => '0');
   sb <= eb(3 downto 0);
   
   a <= signed(ea);
	b <= signed(eb);
   
   eadd <= a - b when cq.es = es_sub else a + b;
	
	eshl <=  ea(11 downto 0)                    when sb = x"0" else
            ea(10 downto 0) & zn(           0) when sb = x"1" else
	         ea( 9 downto 0) & zn( 1 downto  0) when sb = x"2" and sh > 0 else
	         ea( 8 downto 0) & zn( 2 downto  0) when sb = x"3" and sh > 0 else
	         ea( 7 downto 0) & zn( 3 downto  0) when sb = x"4" and sh > 0 else
	         ea( 6 downto 0) & zn( 4 downto  0) when sb = x"5" and sh > 0 else
	         ea( 5 downto 0) & zn( 5 downto  0) when sb = x"6" and sh > 0 else
	         ea( 4 downto 0) & zn( 6 downto  0) when sb = x"7" and sh > 0 else
	         ea( 3 downto 0) & zn( 7 downto  0) when sb = x"8" and sh > 0 else
	         ea( 2 downto 0) & zn( 8 downto  0) when sb = x"9" and sh > 1 else
	         ea( 1 downto 0) & zn( 9 downto  0) when sb = x"A" and sh > 1 else
	         ea(          0) & zn(10 downto  0) when sb = x"B" and sh > 1 else
	                           zn(11 downto  0);
                             
	eshr <=                    ea(11 downto  0) when sb = x"0" else
            sn(10 downto 0) & ea(           0) when sb = x"1" else
	         sn( 9 downto 0) & ea( 1 downto  0) when sb = x"2" and sh > 0 else
	         sn( 8 downto 0) & ea( 2 downto  0) when sb = x"3" and sh > 0 else
	         sn( 7 downto 0) & ea( 3 downto  0) when sb = x"4" and sh > 0 else
	         sn( 6 downto 0) & ea( 4 downto  0) when sb = x"5" and sh > 0 else
	         sn( 5 downto 0) & ea( 5 downto  0) when sb = x"6" and sh > 0 else
	         sn( 4 downto 0) & ea( 6 downto  0) when sb = x"7" and sh > 0 else
	         sn( 3 downto 0) & ea( 7 downto  0) when sb = x"8" and sh > 0 else
	         sn( 2 downto 0) & ea( 8 downto  0) when sb = x"9" and sh > 1 else
            sn( 1 downto 0) & ea( 9 downto  0) when sb = x"A" and sh > 1 else
	         sn(          0) & ea(10 downto  0) when sb = x"B" and sh > 1 else
	                           ea(11 downto  0);
                              
   erol <=  ea(10 downto 0) & ea(          11) when sb = x"1" else
	         ea( 9 downto 0) & ea(11 downto 10) when sb = x"2" and sh > 0 else
	         ea( 8 downto 0) & ea(11 downto  9) when sb = x"3" and sh > 0 else
	         ea( 7 downto 0) & ea(11 downto  8) when sb = x"4" and sh > 0 else
	         ea( 6 downto 0) & ea(11 downto  7) when sb = x"5" and sh > 0 else
	         ea( 5 downto 0) & ea(11 downto  6) when sb = x"6" and sh > 0 else
	         ea( 4 downto 0) & ea(11 downto  5) when sb = x"7" and sh > 0 else
	         ea( 3 downto 0) & ea(11 downto  4) when sb = x"8" and sh > 0 else
	         ea( 2 downto 0) & ea(11 downto  3) when sb = x"9" and sh > 1 else
	         ea( 1 downto 0) & ea(11 downto  2) when sb = x"A" and sh > 1 else
	         ea(          0) & ea(11 downto  1) when sb = x"B" and sh > 1 else
	                           ea(11 downto  0);
   
   eror <=  ea(          0) & ea(11 downto  1) when sb = x"1" else
	         ea( 1 downto 0) & ea(11 downto  2) when sb = x"2" and sh > 0 else
	         ea( 2 downto 0) & ea(11 downto  3) when sb = x"3" and sh > 0 else
	         ea( 3 downto 0) & ea(11 downto  4) when sb = x"4" and sh > 0 else
	         ea( 4 downto 0) & ea(11 downto  5) when sb = x"5" and sh > 0 else
	         ea( 5 downto 0) & ea(11 downto  6) when sb = x"6" and sh > 0 else
	         ea( 6 downto 0) & ea(11 downto  7) when sb = x"7" and sh > 0 else
	         ea( 7 downto 0) & ea(11 downto  8) when sb = x"8" and sh > 0 else
	         ea( 8 downto 0) & ea(11 downto  9) when sb = x"9" and sh > 1 else
	         ea( 9 downto 0) & ea(11 downto 10) when sb = x"A" and sh > 1 else
	         ea(10 downto 0) & ea(          11) when sb = x"B" and sh > 1 else
	                           ea(11 downto  0);
   
   emov <=        eb;
   
   eand <= ea and eb;
   
   eorr <= ea or  eb;
   
   exor <= ea xor eb;
	
	eq <= ea = eb;
	lr <= ea < eb;
	gr <= eq nand lr;
   
   sk <= '1' when (eq and cq.es = es_seq) or (lr and cq.es = es_slr) or (gr and cq.es = es_sgr) else '0';

   with cq.es select ex <=
      eshl when es_shl,
      eshr when es_shr,
      erol when es_rol,
      eror when es_ror,
      emov when es_mov,
      eand when es_and,
      eorr when es_orr,
      exor when es_xor,
      std_logic_vector(eadd) when others;

end rtl;