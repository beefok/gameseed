library ieee;
use ieee.std_logic_1164.all;

package cpu_pkg is

   subtype bctr is std_logic_vector( 2 downto 0);
	subtype bits is std_logic_vector(11 downto 0);
   subtype phya is std_logic_vector(23 downto 0);

   type type_op is
   (  op_err, op_jmp, op_rti, op_irq, op_jri, op_jr,
      op_ldi, op_ld, op_sti, op_st, op_bi, op_bo,
      op_sub, op_adde, op_addi, op_add,
      op_eqe, op_eqi, op_eq,
      op_lre, op_lri, op_lr,
      op_gre, op_gri, op_gr,
      op_shli, op_shl, op_shri, op_shr, op_roli, op_rol, op_rori, op_ror,
      op_me, op_mi, op_mv,
      op_ande, op_andi, op_and,
      op_ore, op_ori, op_or,
      op_xore, op_xori, op_xor
   );

	type type_st is (st_s0, st_s1, st_s2, st_s3);
   type type_ma is (ma_ex, ma_io, ma_ip, ma_ir);
   type type_mb is (mb_no, mb_cr, mb_dr, mb_dw, mb_br, mb_bw);
	type type_md is (md_no, md_s0, md_s1);
   type type_es is (es_seq, es_slr, es_sgr, es_add, es_sub, es_shl, es_shr, es_rol, es_ror, es_mov, es_and, es_orr, es_xor);
   type type_ea is (ea_rx, ea_ip, ea_ry);
   type type_eb is (eb_ry, eb_mi, eb_im, eb_ij);
   type type_ri is (ri_no, ri_ex, ri_ip);
   
   type type_cq is record
		st : type_st;
      ma : type_ma;
      mb : type_mb;
		md : type_md;
      es : type_es;
      ea : type_ea;
      eb : type_eb;
      ri : type_ri;
   end record;

end package cpu_pkg;