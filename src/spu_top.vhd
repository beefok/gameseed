-- channel: MM..VVVV : channel mode, channel volume
-- index  : ...IIIII
-- malance: LLLLRRRR : left and right volume
-- wavesel: ..WWWWWW
-- freq. H: ffffffff
-- freq. L: ffffffff
-- phase H: pppppppp
-- phase L: pppppppp
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity spu_top is port
(  rs, ck : in  std_logic;
   update : in  std_logic;
   mi     : in  std_logic_vector( 7 downto 0);
   ma     : out std_logic_vector( 9 downto 0);
   mo     : out std_logic_vector( 7 downto 0);
   mw     : out std_logic;
   outL   ,
   outR   : out std_logic_vector(23 downto 0)
);
end spu_top;

architecture rtl of spu_top is

   type voice_t is record
      volume: std_logic_vector( 3 downto 0);
      index : std_logic_vector( 4 downto 0);
      bal_l : std_logic_vector( 3 downto 0);
      bal_r : std_logic_vector( 3 downto 0);
      mode  : std_logic_vector( 1 downto 0);
      wave  : std_logic_vector( 5 downto 0);
      freq  : std_logic_vector(15 downto 0);
      phase : std_logic_vector(15 downto 0);
   end record;

   type states is (st_idle, rd_setting, rd_index, rd_balance, rd_wavesel, rd_freq_a, rd_freq_b, rd_phase_a, rd_phase_b, st_calc_0, st_calc_1, st_accum, rd_waveform, st_waveform, wr_index, wr_phase_a, wr_phase_b);
   signal state : states;
   
   signal channel : std_logic_vector(2 downto 0);
   
   signal accL, accR : integer;
   signal wave : std_logic_vector(3 downto 0);
   signal voice : voice_t;

begin

   process (rs, ck)
   begin
   
      if (rs = '0') then
      
         state <= st_idle;
         channel <= "000";
         wave  <= (others => '0');
         mw    <= '0';
         
         voice <= ((others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0') );
      
      elsif (rising_edge(ck)) then
      
         mw <= '0';
      
         case (state) is
            when st_idle =>
            
               accL <= 0;
               accR <= 0;
            
               if (update = '1') then
                  state <= rd_setting;
               end if;
               
            when rd_setting =>
               
               ma           <= "0000" & channel & "000";
               state        <= rd_index;
               
            when rd_index =>
               
               voice.mode   <= mi(7 downto 6);
               voice.volume <= mi(3 downto 0);
               ma           <= "0000" & channel & "001";
               state        <= rd_balance;
               
            when rd_balance =>
               
               voice.index  <= mi(4 downto 0);
               ma           <= "0000" & channel & "010";
               state        <= rd_wavesel;
               
            when rd_wavesel =>
               
               voice.bal_l  <= mi(7 downto 4);
               voice.bal_r  <= mi(3 downto 0);
               ma           <= "0000" & channel & "011";
               state        <= rd_freq_a;
               
            when rd_freq_a =>
               
               voice.wave   <= mi(5 downto 0);
               ma           <= "0000" & channel & "100";
               state        <= rd_freq_b;
               
            when rd_freq_b =>
               
               voice.freq(15 downto 8) <= mi;
               ma           <= "0000" & channel & "101";
               state        <= rd_phase_a;
               
            when rd_phase_a =>
            
               voice.freq( 7 downto 0) <= mi;
               ma           <= "0000" & channel & "110";
               state        <= rd_phase_b;
               
            when rd_phase_b =>
            
               voice.phase(15 downto 8) <= mi;
               ma           <= "0000" & channel & "111";
               state        <= st_calc_0;
               
            when st_calc_0 =>
            
               voice.phase( 7 downto 0) <= mi;
               state        <= st_calc_1;
               
            when st_calc_1 =>
            
               -- reached end of phase counter
               if (voice.phase = x"0000") then
               
                  -- reached top index
                  if (voice.index = "11111") then
                     voice.index <= "00000";
                  else
                     voice.index <= std_logic_vector(to_unsigned(to_integer(unsigned(voice.index)) + 1, 5));
                  end if;
                  
                  -- reload phase counter with frequency
                  voice.phase <= voice.freq;
                  
               -- still counting down on the phase counter
               else
               
                  voice.phase <= std_logic_vector(to_unsigned(to_integer(unsigned(voice.phase)) - 1, 16));
                  
               end if;
               
               state <= rd_waveform;
               
            when rd_waveform =>
            
               ma    <= voice.wave & voice.index(4 downto 1);
               state <= st_waveform;
               
            when st_waveform =>
            
               if (voice.index(0) = '0') then
                  wave <= mi(3 downto 0);
               else
                  wave <= mi(7 downto 4);
               end if;
               
               state <= wr_index;
               
            when wr_index =>
            
               accL  <= accL + (to_integer(unsigned(wave)) * to_integer(unsigned(voice.bal_l and voice.volume)));
               accR  <= accR + (to_integer(unsigned(wave)) * to_integer(unsigned(voice.bal_r and voice.volume)));
            
               ma    <= "0000" & channel & "001";
               mo    <= "000"  & voice.index;
               mw    <= '1';
               state <= wr_phase_a;
               
            when wr_phase_a =>
            
               ma    <= "0000" & channel & "110";
               mo    <= voice.phase( 7 downto 0);
               mw    <= '1';
               state <= wr_phase_b;
               
            when wr_phase_b =>
               
               ma    <= "0000" & channel & "111";
               mo    <= voice.phase(15 downto 8);
               mw    <= '1';
               
               if (channel = "111") then
                  outL    <= std_logic_vector(to_unsigned(accL, 24));
                  outR    <= std_logic_vector(to_unsigned(accR, 24));
                  state   <= st_idle;
                  channel <= "000";
               else
                  state   <= rd_setting;
                  channel <= std_logic_vector(to_unsigned(to_integer(unsigned(channel)) + 1, 3));
               end if;
               
            when others =>
               
               state <= st_idle;
            
         end case;
      
      end if;
   
   end process;

end rtl;