package gpuPkg is

	type palBus is record
		aw, bw	:	std_logic;
		aa, ba	:	palAddr;
		ai, bi	:	palEntry;
		aq, bq	:	palEntry;
	end record;
	
	type chrBus is record
		aw, bw	:	std_logic;
		aa, ba	:	chrAddr;
		ai, bi	:	chrEntry;
		aq, bq	:	chrEntry;
	end record;
	
	type oamBus is record
		aw, bw	:	std_logic;
		aa, ba	:	oamAddr;
		ai, bi	:	oamEntry;
		aq, bq	:	oamEntry;
	end record;
	
	type palAddr	is integer range 0 to 511;
	type chrAddr	is integer range 0 to 2047;
	type oamAddr	is integer range 0 to 2047;

	type oamEntry is std_logic_vector(8 downto 0);
	type chrEntry is std_logic_vector(7 downto 0);
	type palEntry is record
		r, g, b : std_logic_vector(2 downto 0);
	end record;

end gpuPkg;