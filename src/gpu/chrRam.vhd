library ieee;
use ieee.std_logic_1164.all;
use work.gpuPkg.all;

entity chrRam is
port (
	ck				:	in		std_logic;
	aw, bw		:	in		std_logic;
	ai, bi		:	in		chrEntry;
	aa, ba		:	in		chrAddr;
	aq, bq		:	out	chrEntry
);
end chrRam;

architecture rtl of chrRam is

	type chrRam_t is array (0 to 2047) of chrEntry;
	
	signal chr : chrRam_t;

begin

	process (ck) begin
	
		if (rising_edge(ck)) then
		
			if (aw = '1') then chr(aa) <= ai; end if;
			if (bw = '1') then chr(ba) <= bi; end if;
		
		end if;
	
	end process;
	
	aq <= chr(aa);
	bq <= chr(ba);

end rtl;