library ieee;
use ieee.std_logic_1164.all;
use work.gpuPkg.all;

entity oamRam is
port (
	ck				:	in		std_logic;
	aw, bw		:	in		std_logic;
	ai, bi		:	in		oamEntry;
	aa, ba		:	in		oamAddr;
	aq, bq		:	out	oamEntry
);
end oamRam;

architecture rtl of chrRam is

	type oamRam_t is array (0 to 2047) of oamEntry;
	
	signal oam : oamRam_t;

begin

	process (ck) begin
	
		if (rising_edge(ck)) then
		
			if (aw = '1') then oam(aa) <= ai; end if;
			if (bw = '1') then oam(ba) <= bi; end if;
		
		end if;
	
	end process;
	
	aq <= oam(aa);
	bq <= oam(ba);

end rtl;