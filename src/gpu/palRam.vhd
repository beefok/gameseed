library ieee;
use ieee.std_logic_1164.all;
use work.gpuPkg.all;

entity palRam is
port (
	ck				:	in		std_logic;
	aw, bw		:	in		std_logic;
	ai, bi		:	in		palEntry;
	aa, ba		:	in		palAddr;
	aq, bq		:	out	palEntry
);
end palRam;

architecture rtl of chrRam is

	type palRam_t is array (0 to 511) of palEntry;
	
	signal pal : palRam_t;

begin

	process (ck) begin
	
		if (rising_edge(ck)) then
		
			if (aw = '1') then pal(aa) <= ai; end if;
			if (bw = '1') then pal(ba) <= bi; end if;
		
		end if;
	
	end process;
	
	aq <= pal(aa);
	bq <= pal(ba);

end rtl;