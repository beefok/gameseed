library ieee;
use ieee.std_logic_1164.all;
use work.gpuPkg.all;

entity gpu is
port (
	rs, ck		:	in		std_logic;
);
end gpu;

architecture rtl of gpu is

	signal	palA, palB		:	palBus;
	signal	oamA, oamB		:	oamBus;
	signal	chrA, chrB		:	chrBus;

begin

	-- 2 x  512 x 36	###	Palette:
	--	..xxxxxxxxn	colors		rrr ggg bbb rrr ggg bbb rrr ggg bbb rrr ggg bbb rrr ggg bbb rrr ggg bbb rrr ggg bbb rrr ggg bbb
	--
	
	palAi: palRam port map ( .ck => ck,
		.aw => palA.aw, .bw => palA.bw, .ai => palA.ai, .bi => palA.bi,
		.aa => palA.aa, .ba => palA.ba, .aq => palA.aq, .bq => palA.bq
	);
	
	palBi: palRam port map ( .ck => ck,
		.aw => palB.aw, .bw => palB.bw, .ai => palB.ai, .bi => palB.bi,
		.aa => palB.aa, .ba => palB.ba, .aq => palB.aq, .bq => palB.bq
	);
	
	--	4 x 2048 x  8	###	Characters:
	--	xxxxxxxxyyy blocks		abcd abcd abcd abcd abcd abcd abcd abcd
	
	chrAi: chrRam port map ( .ck => ck,
		.aw => chrA.aw, .bw => chrA.bw, .ai => chrA.ai, .bi => chrA.bi,
		.aa => chrA.aa, .ba => chrA.ba, .aq => chrA.aq, .bq => chrA.bq
	);
	chrBi: chrRam port map ( .ck => ck,
		.aw => chrB.aw, .bw => chrB.bw, .ai => chrB.ai, .bi => chrB.bi,
		.aa => chrB.aa, .ba => chrB.ba, .aq => chrB.aq, .bq => chrB.bq
	);
	chrCi: chrRam port map ( .ck => ck,
		.aw => chrC.aw, .bw => chrC.bw, .ai => chrC.ai, .bi => chrC.bi,
		.aa => chrC.aa, .ba => chrC.ba, .aq => chrC.aq, .bq => chrC.bq
	);
	chrDi: chrRam port map ( .ck => ck,
		.aw => chrD.aw, .bw => chrD.bw, .ai => chrD.ai, .bi => chrD.bi,
		.aa => chrD.aa, .ba => chrD.ba, .aq => chrD.aq, .bq => chrD.bq
	);
	
	--
	-- 4 x 2048 x  9	###	Nametable:
	--	000xxxxxxxx objs			..xxxxxxxxxx ..yyyyyyyyyy hvkrrrhhhhhh
	-- 001xxxxxxxx menus			hv cccccccc pppppppp
	-- 01bxxxxxxxn pixels		rrr ggg bbb
	-- 1xxxxxxxxnn tables		hv cccccccc pppppppp
	
	oamAi: oamRam port map ( .ck => ck,
		.aw => oamA.aw, .bw => oamA.bw, .ai => oamA.ai, .bi => oamA.bi,
		.aa => oamA.aa, .ba => oamA.ba, .aq => oamA.aq, .bq => oamA.bq
	);
	oamBi: oamRam port map ( .ck => ck,
		.aw => oamB.aw, .bw => oamB.bw, .ai => oamB.ai, .bi => oamB.bi,
		.aa => oamB.aa, .ba => oamB.ba, .aq => oamB.aq, .bq => oamB.bq
	);
	oamCi: oamRam port map ( .ck => ck,
		.aw => oamC.aw, .bw => oamC.bw, .ai => oamC.ai, .bi => oamC.bi,
		.aa => oamC.aa, .ba => oamC.ba, .aq => oamC.aq, .bq => oamC.bq
	);
	oamDi: oamRam port map ( .ck => ck,
		.aw => oamD.aw, .bw => oamD.bw, .ai => oamD.ai, .bi => oamD.bi,
		.aa => oamD.aa, .ba => oamD.ba, .aq => oamD.aq, .bq => oamD.bq
	);
	
	--	Timing Generator: gpu is ran at 50MHz, 2 clocks per pixel
	--	800 pixels per scanline, 1600 clocks
	-- 288 pixels blank,  512 pixels active
	-- 576 clocks blank, 1024 clocks active
	
	sync: syncGen port map ( .ck => ck, .rs => rs, .ha => ha, .va => va, .hs => hs, .vs => vs );
	
	-- Block Generator:
	-- 0 read  oam					read pix		...
	-- 1 read  idx					read pal		...
	-- 2 read  pix, pal, chr	...			display pal[pix]
	-- 3 write pix					...			...
	
	
	

end rtl;