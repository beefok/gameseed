library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.gpuPkg.all;

entity syncGen is
port (
	rs, ck		:	in		std_logic;
	xp, yp		:	out	std_logic_vector(8 downto 0);
	hs, vs		,
	ha, va		:	out	std_logic
);
end syncGen;

architecture rtl of syncGen is

	signal tk		:	std_logic;
	signal xc, yc	:	integer range 0 to 511;
	signal xp, yp	:	std_logic_vector(8 downto 0);

begin

	-- 512 active, 64 blank, 16 blank, 96 sync, 48 blank, 64 blank
	-- 384 active, 48 blank, 10 blank, 2 sync, 33 blank, 48 blank

	ha <= '1' when xc < 256 else '0';
	va <= '1' when yc < 384 else '0';
	hs <= '1' when xc >= 592 and xc < 688 else '0';
	vs <= '1' when yc >= 442 and yc < 444 else '0';
	yp <= std_logic_vector(to_unsigned(yc, 9));
	xp <= std_logic_vector(to_unsigned(xc, 9));

	process (rs, ck) begin
	
		if (rs = '0') then
			tk <= '0';
			xc <= 0; yc <= 0;
		elsif (rising_edge(ck)) then
			if (tk = '0') then
				tk <= '1';
			else
				if (xc = 799) then
					xc <= 0;
					if (yc = 524) then
						yc <= 0;
					else
						yc <= yc + 1;
					end if;
				else
					xc <= xc + 1;
				end if;
				tk <= '0';
			end if;
		end if;
	end process;

end rtl;