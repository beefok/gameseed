--             cs ns ma    mb    es    ea    eb    ri
-- 00x0  rti   0  0  ex    cr    mov   ..    ..    ..
-- 00xy  irq   0  1  ir    cr    ..    ..    ..    ..
--             1  0  ex    cr    mov   ..    mi    ip
-- 00x4  jr.   0  1  ip    cr    ..    ..    ..    ..
--             1  0  ex    cr    mov   ..    mi    ip
-- 01xy  jr    0  0  ex    cr    mov   ..    ry    ip
-- 02xy  ldi   0  1  ip    cr    ..    ..    ..    ..  
--             1  2  ex    dr    add   ry    mi    ..
--             2  0  ip    cr    mov   ..    mi    ex
-- 03xy  ld    0  1  ex    dr    mov   ..    ry    ..
--             1  0  ip    cr    mov   ..    mi    ex
-- 04xy  sti   0  1  ip    cr    ..    ..    ..    ..  
--             1  2  ex    dw    add   ry    mi    ..
--             2  0  ip    cr    mov   ..    mi    ..
-- 05xy  st    0  1  ex    dw    mov   ..    ry    ..
--             1  0  ip    cr    mov   ..    mi    ..
-- 06xi  bi    0  1  io    dr    ..    ..    ..    ..
--             1  0  ip    cr    mov   ..    mi    ex
-- 06xo  bo    0  1  io    dw    ..    ..    ..    ..
--             1  0  ip    cr    mov   ..    mi    ex
-- 07xy  sub   0  0  ip    cr    sub   rx    ry    ex
-- 10xy  addi  0  0  ip    cr    add   rx    im    ex
-- 10x4  add.  0  1  ip    cr    ..    ..    ..    ..
--             1  0  ip    cr    add   rx    mi    ex
-- 11xy  add   0  0  ip    cr    add   rx    ry    ex
-- 12xy  eqi   0  0  ip    cr    seq   rx    im    ..
-- 12x4  eq.   0  1  ip    cr    ..    ..    ..    ..
--             1  0  ip    cr    seq   rx    mi    ..
-- 13xy  eq    0  0  ip    cr    seq   rx    ry    ..
-- 14xy  lri   0  0  ip    cr    slr   rx    im    ..
-- 14x4  lr.   0  1  ip    cr    ..    ..    ..    ..
--             1  0  ip    cr    slr   rx    mi    ..
-- 15xy  lr    0  0  ip    cr    slr   rx    ry    ..
-- 16xy  gri   0  0  ip    cr    sgr   rx    im    ..
-- 16x4  gr.   0  1  ip    cr    ..    ..    ..    ..
--             1  0  ip    cr    sgr   rx    mi    ..
-- 17xy  gr    0  0  ip    cr    sgr   rx    ry    ..
-- 20xy  shli  0  0  ip    cr    shl   rx    im    ex
-- 21xy  shl   0  0  ip    cr    shl   rx    ry    ex
-- 22xy  shri  0  0  ip    cr    shr   rx    im    ex
-- 23xy  shr   0  0  ip    cr    shr   rx    ry    ex
-- 24xy  roli  0  0  ip    cr    rol   rx    im    ex
-- 25xy  rol   0  0  ip    cr    rol   rx    ry    ex
-- 26xy  rori  0  0  ip    cr    ror   rx    im    ex
-- 27xy  ror   0  0  ip    cr    ror   rx    ry    ex
-- 30xy  mi    0  0  ip    cr    mov   rx    im    ex
-- 30x4  mi.   0  1  ip    cr    ..    ..    ..    ..
--             1  0  ip    cr    mov   rx    mi    ex
-- 31xy  mv    0  0  ip    cr    mov   rx    ry    ex
-- 32xy  andi  0  0  ip    cr    and   rx    im    ex
-- 32x4  and.  0  1  ip    cr    ..    ..    ..    ..
--             1  0  ip    cr    and   rx    mi    ex
-- 33xy  and   0  0  ip    cr    and   rx    ry    ex
-- 34xy  ori   0  0  ip    cr    orr   rx    im    ex
-- 34x4  or.   0  1  ip    cr    ..    ..    ..    ..
--             1  0  ip    cr    orr   rx    mi    ex
-- 35xy  or    0  0  ip    cr    orr   rx    ry    ex
-- 36xy  xori  0  0  ip    cr    xor   rx    im    ex
-- 36x4  xor.  0  1  ip    cr    ..    ..    ..    ..
--             1  0  ip    cr    xor   rx    mi    ex
-- 37xy  xor   0  0  ip    cr    xor   rx    ry    ex
-- >jjj  j     0  0  ex    cr    add   ip    ij    ..
--
-- ma := ir    mb := cr    es := nop
--       io          dr          seq
--       ex          dw          slr
--       ip          ..          sgr
--                               nop
-- ea := rx    eb := ry          nop
--       ry          mi          add
--       ip          im          sub
--       ..          ij          shl
--                               shr
-- ri := no    ns := s0          rol
--       ex          s1          ror
--       ip          s2          mov
--       ..          s3          and
--                               orr
--                               xor
-- total state: 16 bits

library ieee;
use ieee.std_logic_1164.all;
use work.cpu_pkg.all;

entity cpu_ctr is port
(  st : in  type_st;
   md : in  std_logic;
	sk : in  std_logic;
   iw : in  bits;
   cq : out type_cq
);
end cpu_ctr;

architecture rtl of cpu_ctr is

   signal st0, st1, st2, st3	: boolean;
	signal sk0, sk1        	   : boolean;
   signal ij, bi, bo       	: boolean;
   signal ye, yr           	: boolean;
   signal irqmode, usrmode 	: boolean;
   
   signal op               	: type_op;
   signal sn               	: std_logic_vector(4 downto 0);
   signal sy           	      : std_logic_vector(2 downto 0);
   
begin

   st0 <= st = st_s0;
   st1 <= st = st_s1;
   st2 <= st = st_s2;
	st3 <= st = st_s3;
	sk0 <= sk = '0';
	sk1 <= sk = '1';
   
   sn <= iw(10 downto 6);
   sy <= iw( 2 downto 0);
   
   ij <= iw(11) = '1';  -- jump instruction has the 11th bit high
   ye <= sy  = "100";   -- y operand is external immediate
   yr <= sy /= "100";   -- y operand is not external immediate
   bi <= iw( 2) = '0';  -- bus in
   bo <= iw( 2) = '1';  -- bus out
   
   irqmode <= md = '0';
   usrmode <= md = '1';

   op <= op_jmp   when ij else
         op_rti   when sn = "00000" and yr and irqmode else
         op_irq   when sn = "00000" and yr and usrmode else
         op_jri   when sn = "00000" and ye else
         op_jr    when sn = "00001" else
         op_ldi   when sn = "00010" else
         op_ld    when sn = "00011" else
         op_sti   when sn = "00100" else
         op_st    when sn = "00101" else
         op_bi    when sn = "00110" and bi else
         op_bo    when sn = "00110" and bo else
         op_sub   when sn = "00111" else
         op_adde  when sn = "01000" and ye else
         op_addi  when sn = "01000" and yr else
         op_add   when sn = "01001" else
         op_eqe   when sn = "01010" and ye else
         op_eqi   when sn = "01010" and yr else
         op_eq    when sn = "01011" else
         op_lre   when sn = "01100" and ye else
         op_lri   when sn = "01100" and yr else
         op_lr    when sn = "01101" else
         op_gre   when sn = "01110" and ye else
         op_gri   when sn = "01110" and yr else
         op_gr    when sn = "01111" else
         op_shli  when sn = "10000" else
         op_shl   when sn = "10001" else
         op_shri  when sn = "10010" else
         op_shr   when sn = "10011" else
         op_roli  when sn = "10100" else
         op_rol   when sn = "10101" else
         op_rori  when sn = "10110" else
         op_ror   when sn = "10111" else
         op_me    when sn = "11000" and ye else
         op_mi    when sn = "11000" and yr else
         op_mv    when sn = "11001" else
         op_ande  when sn = "11010" and ye else
         op_andi  when sn = "11010" and yr else
         op_and   when sn = "11011" else
         op_ore   when sn = "11100" and ye else
         op_ori   when sn = "11100" and yr else
         op_or    when sn = "11101" else
         op_xore  when sn = "11110" and ye else
         op_xori  when sn = "11110" and yr else
         op_xor   when sn = "11111" else
         op_err;
   
	cq.st <= st_s1 when  (op = op_irq  and st0) or
								(op = op_jri  and st0) or
								(op = op_ldi  and st0) or
								(op = op_sti  and st0) or
								(op = op_ld   and st0) or
								(op = op_st   and st0) or
								(op = op_bi   and st0) or
								(op = op_bo   and st0) or
								(op = op_adde and st0) or
								(op = op_eqe  and st0) or
								(op = op_lre  and st0) or
								(op = op_gre  and st0) or
								(op = op_me   and st0) or
								(op = op_ande and st0) or
								(op = op_ore  and st0) or
								(op = op_xore and st0) else
				st_s2 when  (op = op_ldi  and st1) or
								(op = op_sti  and st1) else
				st_s3 when	(op = op_err         ) else
				st_s0;
	
   cq.ma <= ma_ip when  (sk1) else
				ma_ex when  (op = op_jmp  and st0) or
								(op = op_rti  and st0) or
                        (op = op_irq  and st1) or
                        (op = op_jri  and st1) or
                        (op = op_st   and st0) or
                        (op = op_sti  and st1) or
                        (op = op_ld   and st0) or
                        (op = op_ldi  and st1) else
            ma_io when  (op = op_bi   and st0) or
                        (op = op_bo   and st0) else
            ma_ir when  (op = op_irq  and st0) else
            ma_ip;

   cq.mb <= mb_cr when  (sk1) else
				mb_dr when  (op = op_irq  and st0) or
								(op = op_ld   and st0) or
                        (op = op_ldi  and st1) else
            mb_br when  (op = op_bi   and st0) else
            mb_dw when  (op = op_st   and st0) or
                        (op = op_sti  and st1) else
            mb_bw when  (op = op_bo   and st0) else
            mb_cr;
				
	cq.md <= md_no when  (sk1) else
				md_s0 when  (op = op_irq  and st1) else
				md_s1 when  (op = op_rti  and st0) else
				md_no;

   cq.es <= es_add when (op = op_jmp  and st0) or
                        (op = op_ldi  and st1) or
                        (op = op_sti  and st1) or
                        (op = op_addi and st0) or
                        (op = op_adde and st1) or
                        (op = op_add  and st0) else
            es_sub when (op = op_sub  and st0) else
            es_seq when (op = op_eqi  and st0) or
                        (op = op_eqe  and st1) or
                        (op = op_eq   and st0) else
            es_slr when (op = op_lri  and st0) or
                        (op = op_lre  and st1) or
                        (op = op_lr   and st0) else
            es_sgr when (op = op_gri  and st0) or
                        (op = op_gre  and st1) or
                        (op = op_gr   and st0) else
            es_shl when (op = op_shl  and st0) or
                        (op = op_shli and st0) else
            es_shr when (op = op_shr  and st0) or
                        (op = op_shri and st0) else
            es_rol when (op = op_rol  and st0) or
                        (op = op_roli and st0) else
            es_ror when (op = op_ror  and st0) or
                        (op = op_rori and st0) else
            es_and when (op = op_andi and st0) or
                        (op = op_ande and st1) or
                        (op = op_and  and st0) else
            es_orr when (op = op_ori  and st0) or
                        (op = op_ore  and st1) or
                        (op = op_or   and st0) else
            es_xor when (op = op_xori and st0) or
                        (op = op_xore and st1) or
                        (op = op_xor  and st0) else
            es_mov;
            
   cq.ea <= ea_ry when  (op = op_ldi  and st1) or
                        (op = op_sti  and st1) else
            ea_ip when  (op = op_jmp  and st0) else
            ea_rx;
            
   cq.eb <= eb_ij when  (op = op_jmp  and st0) else
            eb_im when  (op = op_addi and st0) or
                        (op = op_shli and st0) or
                        (op = op_shri and st0) or
                        (op = op_roli and st0) or
                        (op = op_rori and st0) or
                        (op = op_mi   and st0) or
                        (op = op_andi and st0) or
                        (op = op_ori  and st0) or
                        (op = op_xori and st0) else
            eb_mi when  (op = op_irq  and st1) or
								(op = op_adde and st1) or
                        (op = op_eqe  and st1) or
                        (op = op_lre  and st1) or
                        (op = op_gre  and st1) or
                        (op = op_adde and st1) or
                        (op = op_me   and st1) or
                        (op = op_ande and st1) or
                        (op = op_ore  and st1) or
                        (op = op_xore and st1) or
                        (op = op_ldi  and not st0) or
                        (op = op_sti  and not st0) else
            eb_ry;
            
   cq.ri <= ri_no when  (sk1) else
				ri_ip when  (op = op_irq  and st1) or
                        (op = op_jri  and st1) or
                        (op = op_jr   and st0) else
            ri_ex when  (op = op_ldi  and st2) or
                        (op = op_ld   and st1) or
                        (op = op_bi   and st1) or
                        (op = op_sub  and st0) or
                        (op = op_add  and st0) or
                        (op = op_shl  and st0) or
                        (op = op_shr  and st0) or
                        (op = op_rol  and st0) or
                        (op = op_ror  and st0) or
                        (op = op_mv   and st0) or
                        (op = op_and  and st0) or
                        (op = op_or   and st0) or
                        (op = op_xor  and st0) or
                        (op = op_addi and st0) or
                        (op = op_shli and st0) or
                        (op = op_shri and st0) or
                        (op = op_roli and st0) or
                        (op = op_rori and st0) or
                        (op = op_mi   and st0) or
                        (op = op_andi and st0) or
                        (op = op_ori  and st0) or
                        (op = op_xori and st0) or
                        (op = op_adde and st1) or
                        (op = op_me   and st1) or
                        (op = op_ande and st1) or
                        (op = op_ore  and st1) or
                        (op = op_xore and st1) else
            ri_no;

end rtl;