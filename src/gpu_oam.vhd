library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;

library std;
use std.textio.all;
use work.cpu_pkg.all;

entity gpu_oam is port
(  ck, rs         : in  std_logic;
   md             : in  std_logic;
-- gpu interface
   aw, bw, cw, dw : in  std_logic;
   aa, ba, ca, da : in  std_logic_vector( 9 downto 0);
   ai, bi, ci, di : in  std_logic_vector( 8 downto 0);
   ao, bo, co, do : out std_logic_vector( 8 downto 0);
-- dma controller interface
   pw, pr         : in  std_logic;
   pa             : in  phya;
   pi             : in  bits;
   po             : out bits
);
end gpu_oam;

architecture rtl of gpu_oam is

	type type_ram is array (0 to 1023) of std_logic_vector(8 downto 0);

	function init_rom(filename : string) return type_ram is
      file rom_file   : text open read_mode is filename;
      variable ret    : type_ram;
      variable ln     : line;
   begin
      for i in 0 to 1023 loop
			if (endfile(rom_file)) then
				ret(i) := (others => '0');
			else
				readline(rom_file, ln);
				oread(ln, ret(i));
			end if;
      end loop;
        
		return ret;
   end function init_rom;

   signal oam0 : type_ram := init_rom("oam0.bin");
   signal oam1 : type_ram := init_rom("oam1.bin");

begin

   process (ck, rs)
   begin
      if (rising_edge(ck)) then
      
         if (rs = '0') then

            ao <= (others => '0');
            bo <= (others => '0');
            co <= (others => '0');
            do <= (others => '0');
            po <= (others => '0');
            
         else
         
            -- gpu controller mode
            if (md = '0') then
               
               if (aw = '1') then oam0(to_integer(unsigned(aa))) <= ai; end if;
               ao <= oam0(to_integer(unsigned(aa)));
               
               if (bw = '1') then oam1(to_integer(unsigned(ba))) <= bi; end if;
               bo <= oam0(to_integer(unsigned(ba)));
               
               if (cw = '1') then oam0(to_integer(unsigned(ca))) <= ci; end if;
               co <= oam1(to_integer(unsigned(ca)));
               
               if (dw = '1') then oam1(to_integer(unsigned(da))) <= di; end if;
               do <= oam1(to_integer(unsigned(da)));
            
            -- dma controller mode
            else
            
               if (pa(0) = '0') then
                  if (pw = '1') then oam0(to_integer(unsigned(pa(10 downto 1)))) <= pi; end if;
                  po <= oam0(to_integer(unsigned(pa(10 downto 1))));
               else
                  if (pw = '1') then oam1(to_integer(unsigned(pa(10 downto 1)))) <= pi; end if;
                  po <= oam1(to_integer(unsigned(pa(10 downto 1))));
               end if;
            
            end if;
            
         end if;

      end if;
   end process;

end rtl;