library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.cpu_pkg.all;

entity cpu_main is port
(  ir : in  std_logic_vector( 3 downto 0);
   ck : in  std_logic;
   mw : out std_logic;
   ma : out std_logic_vector(18 downto 0);
   mo : out std_logic_vector(11 downto 0);
   mi : in  std_logic_vector(11 downto 0)
);
end cpu_main;

architecture rtl of cpu_main is

   component cpu_ctrl is port
   (  ir : in  std_logic_vector( 3 downto 0);
      ck : in  std_logic;
      mi : in  std_logic_vector(11 downto 0);
      md : out std_logic_vector(11 downto 0);
      tr : out std_logic_vector(11 downto 0);
      rs : out std_logic;
      cr : out cr_t;
      nr : out cr_t
   );
   end component;
   
   component cpu_regs is port
   (  rs : in  std_logic;
      ck : in  std_logic;
      cr : in  cr_t;       -- read  control (clock 0)
      nr : in  cr_t;       -- write control (clock 1)
      md : in  std_logic_vector(11 downto 0);
      dx : in  std_logic_vector(11 downto 0);
      dy : in  std_logic_vector(11 downto 0);
      ea : out std_logic_vector(11 downto 0);
      eb : out std_logic_vector(11 downto 0);
      bq : out std_logic_vector( 6 downto 0)
   );
   end component;
   
   component cpu_exec is port
   (  rs : in  std_logic;
      ck : in  std_logic;
      nr : in  cr_t;
      ea : in  std_logic_vector(11 downto 0);
      eb : in  std_logic_vector(11 downto 0);
      sk : out std_logic;
      ex : out std_logic_vector(11 downto 0)
   );
   end component;
   
   component cpu_next is port
   (  rs : in  std_logic;
      ck : in  std_logic;
      nr : in  cr_t;
      sk : in  std_logic;
      ea : in  std_logic_vector(11 downto 0);
      eb : in  std_logic_vector(11 downto 0);
      ex : in  std_logic_vector(11 downto 0);
      bp : in  std_logic_vector( 6 downto 0);
      tr : in  std_logic_vector(11 downto 0); 
      ma : out std_logic_vector(18 downto 0);
      mw : out std_logic;
      mo : out std_logic_vector(11 downto 0);
      dx : out std_logic_vector(11 downto 0);
      dy : out std_logic_vector(11 downto 0)
   );
   end component;
   
   -- control unit signals (ctrl -> regs)
   signal md : std_logic_vector(11 downto 0);
   signal tr : std_logic_vector(11 downto 0);
   signal rs : std_logic;
   signal cr : cr_t;
   signal nr : cr_t;
   
   -- register unit signals (regs -> exec / next)
   signal dx : std_logic_vector(11 downto 0);
   signal dy : std_logic_vector(11 downto 0);
   signal ea : std_logic_vector(11 downto 0);
   signal eb : std_logic_vector(11 downto 0);
   signal bp : std_logic_vector( 6 downto 0);
   
   -- execute unit signals
   signal sk : std_logic;
   signal ex : std_logic_vector(11 downto 0);

begin

   -- control unit
   u0 : cpu_ctrl port map
   (  ir => ir,
      ck => ck,
      mi => mi,
      md => md,
      tr => tr,
      rs => rs,
      cr => cr,
      nr => nr
   );
   
   -- register unit
   u1 : cpu_regs port map
   (  rs => rs,
      ck => ck,
      cr => cr,
      nr => nr,
      md => md,
      dx => dx,
      dy => dy,
      ea => ea,
      eb => eb,
      bq => bp
   );
   
   -- execute unit
   u2 : cpu_exec port map
   (  rs => rs,
      ck => ck,
      nr => nr,
      ea => ea,
      eb => eb,
      sk => sk,
      ex => ex
   );
   
   -- fetch unit
   u3 : cpu_next port map
   (  rs => rs,
      ck => ck,
      nr => nr,
      sk => sk,
      ea => ea,
      eb => eb,
      ex => ex,
      bp => bp,
      tr => tr,	
      ma => ma,
      mw => mw,
      mo => mo,
      dx => dx,
      dy => dy
   );
   
end rtl;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.cpu_pkg.all;

entity tbu_main is
end tbu_main;

architecture tb of tbu_main is

   component cpu_main is port
   (  ir : in  std_logic_vector( 3 downto 0);
      ck : in  std_logic;
      mw : out std_logic;
      ma : out std_logic_vector(18 downto 0);
      mo : out std_logic_vector(11 downto 0);
      mi : in  std_logic_vector(11 downto 0)
   );
   end component;

   signal ir     : std_logic_vector( 3 downto 0);
   signal ck     : std_logic;
   signal mw     : std_logic;
   signal ma     : std_logic_vector(18 downto 0);
   signal mi     : std_logic_vector(11 downto 0);
   signal mo     : std_logic_vector(11 downto 0);

begin	

   reset: process
   begin
      ir <= "0000";
      wait for 200 ns;
      ir <= "1111";
      wait;
   end process;

   clock: process
   begin
      ck <= '0';
      wait for 50 ns;
      ck <= '1';
      wait for 50 ns;
   end process;

   test: process (ir(0), ck)
   begin
      if (ir(0) = '0') then
      
         mi <= (others => '0');
         
      elsif (rising_edge(ck)) then

         case (ma(5 downto 0)) is
            when o"00"  => mi <= x"000";
            when o"01"  => mi <= x"C0F";
            when o"02"  => mi <= x"ABC";
            when o"03"  => mi <= x"C1F";
            when o"04"  => mi <= x"DEF";
            when others => mi <= x"0FF";
         end case;

      end if;
   end process;		

   uut: cpu_main port map
   (  ir => ir,
      ck => ck,
      mw => mw,
      ma => ma,
      mo => mo,
      mi => mi
   );

end tb;