library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.cpu_pkg.all;

entity cpu_regs is port
(  rs : in  std_logic;
   ck : in  std_logic;
   cr : in  cr_t;       -- read  control (clock 0)
   nr : in  cr_t;       -- write control (clock 1)
   md : in  std_logic_vector(11 downto 0);
   dx : in  std_logic_vector(11 downto 0);
   dy : in  std_logic_vector(11 downto 0);
   ea : out std_logic_vector(11 downto 0);
   eb : out std_logic_vector(11 downto 0);
   bq : out std_logic_vector( 6 downto 0)
);
end cpu_regs;

architecture rtl of cpu_regs is

   component SB_RAM256x16 is
   generic
   (  INIT_0 : std_logic_vector(255 downto 0);
      INIT_1 : std_logic_vector(255 downto 0);
      INIT_2 : std_logic_vector(255 downto 0);
      INIT_3 : std_logic_vector(255 downto 0);
      INIT_4 : std_logic_vector(255 downto 0);
      INIT_5 : std_logic_vector(255 downto 0);
      INIT_6 : std_logic_vector(255 downto 0);
      INIT_7 : std_logic_vector(255 downto 0);
      INIT_8 : std_logic_vector(255 downto 0);
      INIT_9 : std_logic_vector(255 downto 0);
      INIT_A : std_logic_vector(255 downto 0);
      INIT_B : std_logic_vector(255 downto 0);
      INIT_C : std_logic_vector(255 downto 0);
      INIT_D : std_logic_vector(255 downto 0);
      INIT_E : std_logic_vector(255 downto 0);
      INIT_F : std_logic_vector(255 downto 0)
   );
   port
   (  RDATA : out std_logic_vector(15 downto 0);
      RADDR : in  std_logic_vector( 7 downto 0);
      RCLK  : in  std_logic;
      RCLKE : in  std_logic;
      RE    : in  std_logic;
      WDATA : in  std_logic_vector(15 downto 0);
      WADDR : in  std_logic_vector( 7 downto 0);
      WCLK  : in  std_logic;
      WCLKE : in  std_logic;
      MASK  : in  std_logic_vector(15 downto 0);
      WE    : in  std_logic
   );
   end component;
   
   signal bp : std_logic_vector( 6 downto 0);
   signal gp : std_logic_vector( 4 downto 0);
   signal sp : std_logic_vector( 7 downto 0);
   signal wr : std_logic;
   
   signal xsi, xsx, xsy : std_logic_vector( 7 downto 0);
   signal xgi, xgx, xgy : std_logic_vector( 7 downto 0);
   signal usp           : unsigned( 7 downto 0);
   signal usi, usx, usy : unsigned( 7 downto 0);
   signal ugi, ugx, ugy : unsigned( 7 downto 0);
   
   signal ai, ax, ay : std_logic_vector( 7 downto 0);
   signal s3         : std_logic_vector(11 downto 0);
   
   signal ix, iy     : std_logic_vector(15 downto 0);
   signal rx, ry     : std_logic_vector(15 downto 0);

begin

   ram_x : SB_RAM256x16
   generic map (
      INIT_0 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_8 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_9 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_F => X"0000000000000000000000000000000000000000000000000000000000000000"
   )
   port map (
      RCLK  => ck,  WCLK  => ck,
      RCLKE => '1', WCLKE => wr, MASK  => x"0000",
      RE    => '1', WE    => wr,
      -- register read x
      RADDR => ax, RDATA => rx,
      -- register write x
      WADDR => ai, WDATA => ix
   );
   
   ram_y : SB_RAM256x16
   generic map (
      INIT_0 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_8 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_9 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_F => X"0000000000000000000000000000000000000000000000000000000000000000"
   )
   port map (
      RCLK  => ck,  WCLK  => ck,
      RCLKE => '1', WCLKE => '1', MASK  => x"0000",
      RE    => '1', WE    => wr,
      -- register read y
      RADDR => ay, RDATA => ry,
      -- register write y
      WADDR => ai, WDATA => iy
   );

   -- wr states:
   --    00 : rd return address
   --    01 : wr return address
   --    10 : rd register file
   --    11 : wr register file
   
   ix  <= "0000" & dx;
   iy  <= "0000" & dy;
   
   wr  <= '1' when nr.ri = ri_ex or nr.ri = ri_rt else '0';
   
   usp <= unsigned(sp);
   xgi <=      gp & cr.sx(2 downto 0);    ugi <= unsigned(xgi);
   xgx <=      gp & cr.sx(2 downto 0);    ugx <= unsigned(xgx);
   xgy <=      gp & cr.sy(2 downto 0);    ugy <= unsigned(xgy);
   xsi <= "00000" & cr.sx(2 downto 0);    usi <= unsigned(xsi);
   xsx <= "00000" & cr.sx(2 downto 0);    usx <= unsigned(xsx);
   xsy <= "00000" & cr.sy(2 downto 0);    usy <= unsigned(xsy);

   ai <= std_logic_vector(usp -   1)   when nr.ri = ri_rt  else
         std_logic_vector(      ugi)   when nr.sx(3) = '1' else
         std_logic_vector(usp + usi);
   
   ax <= std_logic_vector(usp -   1)   when cr.eb = eb_rt  else
         std_logic_vector(      ugx)   when cr.sx(3) = '1' else
         std_logic_vector(usp + usx);
         
   ay <= std_logic_vector(usp -   1)   when cr.eb = eb_rt  else
         std_logic_vector(      ugy)   when cr.sy(3) = '1' else
         std_logic_vector(usp + usy);
         

   process (rs, ck)
   begin
   
      if (rs = '0') then
      
         bp <= (others => '0');
         gp <= (others => '0');
         sp <= (others => '0');
         
      elsif (rising_edge(ck)) then

         if (nr.ri = ri_ex) then
            if (nr.sx = x"F") then
               bp <= dy( 6 downto 0);
               gp <= dy(11 downto 7);
            elsif (nr.sx = x"E") then
               sp <= dy( 7 downto 0);
            end if;
         end if;
      
      end if;
   
   end process;
   
   -- output base register
   bq <= bp;
   
   -- execution unit a
   ea <=   gp & bp   when nr.sx = x"F"  else
         x"0" & sp   when nr.sx = x"E"  else
         rx(11 downto 0);
   
   -- execution unit b
   s3 <= o"777" & nr.sy(2 downto 0) when nr.sy(2) = '1' else
         o"000" & nr.sy(2 downto 0);
         
   eb <= s3          when nr.eb = eb_s3 else
         md          when nr.eb = eb_mi else
         gp & bp     when nr.sy = x"F"  else
         x"0" & sp   when nr.sy = x"E"  else
         ry(11 downto 0);

end rtl;

-- library ieee;
-- use ieee.std_logic_1164.all;
-- use ieee.numeric_std.all;

-- entity tbu_regs is
-- end tbu_regs;

-- architecture tb of tbu_regs is

   -- component cpu_regs is port
   -- (  rs : in  std_logic;
      -- ck : in  std_logic;
      -- wr : in  std_logic_vector( 1 downto 0);
      -- dx : in  std_logic_vector(11 downto 0);
      -- dy : in  std_logic_vector(11 downto 0);
      -- si : in  std_logic_vector( 3 downto 0);
      -- sx : in  std_logic_vector( 3 downto 0);
      -- sy : in  std_logic_vector( 3 downto 0);
      -- qx : out std_logic_vector(11 downto 0);
      -- qy : out std_logic_vector(11 downto 0);
      -- qb : out std_logic_vector( 6 downto 0)
   -- );
   -- end component;
   
   -- signal rs, ck     : std_logic;
   -- signal wr         : std_logic_vector( 1 downto 0);
   -- signal dx, dy     : std_logic_vector(11 downto 0);
   -- signal si, sx, sy : std_logic_vector( 3 downto 0);
   -- signal qx, qy     : std_logic_vector(11 downto 0);
   -- signal qb         : std_logic_vector( 6 downto 0);
   
   -- signal st         : integer range 0 to 255;

-- begin

   -- uut: cpu_regs port map
   -- (  rs => rs, ck => ck, wr => wr,
      -- dx => dx, dy => dy,
      -- si => si, sx => sx, sy => sy,
      -- qx => qx, qy => qy, qb => qb
   -- );

   -- reset: process
   -- begin
      -- rs <= '0';
      -- wait for 200 ns;
      -- rs <= '1';
      -- wait;
   -- end process;

   -- clock: process
   -- begin
      -- ck <= '0';
      -- wait for 50 ns;
      -- ck <= '1';
      -- wait for 50 ns;
   -- end process;
   
   -- test: process (rs, ck)
   -- begin
      -- if (rs = '0') then
         -- st <= 0;
         -- dx <= (others => '0');
         -- dy <= (others => '0');
         -- si <= (others => '0');
         -- sx <= (others => '0');
         -- sy <= (others => '0');
         -- wr <= (others => '0');
      -- elsif (rising_edge(ck)) then
      
         -- case (st) is
            -- when 0      => wr <= "11"; dx <= x"ABC"; dy <= x"ABC"; si <= x"0"; sx <= x"0"; sy <= x"0"; -- write local register
            -- when 2      => wr <= "11"; dx <= x"DEF"; dy <= x"DEF"; si <= x"1"; sx <= x"1"; sy <= x"1"; -- write another local register
            -- when 4      => wr <= "01"; dx <= x"000"; dy <= x"010"; si <= x"0"; sx <= x"0"; sy <= x"0"; -- write return address
            -- when 5      => wr <= "00";                                                                 -- read return address
            -- when 6      => wr <= "11"; dx <= x"000"; dy <= x"040"; si <= x"E"; sx <= x"E"; sy <= x"E"; -- write stack register
            -- when 8      => wr <= "11"; dx <= x"000"; dy <= x"362"; si <= x"F"; sx <= x"F"; sy <= x"F"; -- write global / base register
            -- when 9      => wr <= "10";                                         sx <= x"8"; sy <= x"B"; -- read in a global register
            -- when 10     => wr <= "11"; dx <= x"BEE"; dy <= x"BEE"; si <= x"9"; sx <= x"9"; sy <= x"A"; -- write a global register
            -- when others => wr <= "10";
         -- end case;
         
         -- st <= st + 1;
      -- end if;
   -- end process;
   
-- end tb;
