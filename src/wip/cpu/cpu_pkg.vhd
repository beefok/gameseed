library ieee;
use ieee.std_logic_1164.all;

package cpu_pkg is

   type nx_t is (nx_0, nx_1, nx_2, nx_3);
   type ma_t is (ma_ip, ma_u6, ma_jumpr, ma_eaebr, ma_trebr, ma_bpebr, ma_bpebw);
   type sk_t is (sk_nop, sk_ceq, sk_clr, sk_cgr, sk_cry);
   type es_t is (es_mov, es_rol, es_ror, es_asr, es_lsl, es_lsr, es_inc, es_dec, es_add, es_sub, es_and, es_orr, es_xor);
   type eb_t is (eb_ry, eb_mi, eb_rt, eb_s3);
   type ri_t is (ri_no, ri_rt, ri_ex);
   
   type cr_t is record
      nx : nx_t;
      ma : ma_t;
      sk : sk_t;
      es : es_t;
      eb : eb_t;
      ri : ri_t;
      sx : std_logic_vector( 3 downto 0);
      sy : std_logic_vector( 3 downto 0);
   end record;

end package;