library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.cpu_pkg.all;

entity cpu_next is port
(  rs : in  std_logic;
   ck : in  std_logic;
   nr : in  cr_t;
   sk : in  std_logic;
   ea : in  std_logic_vector(11 downto 0);
   eb : in  std_logic_vector(11 downto 0);
   ex : in  std_logic_vector(11 downto 0);
   bp : in  std_logic_vector( 6 downto 0);
   tr : in  std_logic_vector(11 downto 0);
   ma : out std_logic_vector(18 downto 0);
   mw : out std_logic;
   mo : out std_logic_vector(11 downto 0);
   dx : out std_logic_vector(11 downto 0);
   dy : out std_logic_vector(11 downto 0)
);
end cpu_next;

architecture rtl of cpu_next is

   signal sn : std_logic_vector( 9 downto 0);   -- upper sign concat for signed 8-bit jump
   signal s8 : std_logic_vector(17 downto 0);   -- signed 8-bit jump
   signal ja : unsigned(17 downto 0);           -- jump address
   signal ip : std_logic_vector(17 downto 0);   -- instruction pointer
   signal na : std_logic_vector(18 downto 0);   -- next memory address
   
   signal iru6, eaeb, treb, bpeb : std_logic_vector(18 downto 0);

begin

   -- select register write input
   dx <=            ip(11 downto 0) when nr.ri = ri_rt else ex;
   dy <= "000000" & ip( 5 downto 0) when nr.ri = ri_rt else ex;

   -- generate s8 sign
   sn <= "11" & x"FF" when tr(7) = '1' else "00" & x"00";
   -- generate s8
   s8 <= sn & tr( 7 downto 0);
   -- generate jump address
   ja <= unsigned(ip) + unsigned(s8)      when nr.ma = ma_jumpr and sk = '0' else
         unsigned(ip) + 1;
         
   iru6 <= x"000" & tr( 5 downto 0) & "0";
   eaeb <= "0"    & ea( 5 downto 0) & eb;
   treb <= "0"    & nr.sx( 1 downto 0) & nr.sy & eb;
   bpeb <= bp & eb;

   -- generate the next memory address
   with nr.ma select
   na <= iru6  when  ma_u6,
         eaeb  when  ma_eaebr,
         treb  when  ma_trebr,
         bpeb  when  ma_bpebr|
                     ma_bpebw,
     "0" & ip  when  others;
   
   process (rs, ck)
   begin
      if (rs = '0') then
      
         ip <= (others => '0');
         ma <= (others => '0');
         mo <= (others => '0');
         mw <= '0';
         
      elsif (rising_edge(ck)) then
      
         -- memory write
         if (nr.ma = ma_bpebw and sk = '0') then
            mw <= '1';
            mo <=  ea;
         else
            mw <= '0';
         end if;
         
         -- latch the next memory address
         ma <= na;
         
         -- latch the next instruction pointer
         case (nr.ma) is
            when ma_bpebr  |
                 ma_bpebw  => null;
            when others    => ip <= std_logic_vector(ja);
         end case;
         
      end if;
   end process;

end rtl;
