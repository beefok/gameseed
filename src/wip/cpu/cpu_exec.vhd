library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.cpu_pkg.all;

entity cpu_exec is port
(  rs : in  std_logic;
   ck : in  std_logic;
   nr : in  cr_t;
   ea : in  std_logic_vector(11 downto 0);
   eb : in  std_logic_vector(11 downto 0);
   sk : out std_logic;
   ex : out std_logic_vector(11 downto 0)
);
end cpu_exec;

architecture rtl of cpu_exec is
							  
   signal ceq, clr, cgr, cry : boolean;
   signal adder : std_logic_vector(12 downto 0);  
   signal nk : std_logic;

begin	   
	
   -- skip output
   sk <= nk;

   -- compare conditions
   ceq <= ea = eb;
   clr <= ea < eb;
   cgr <= not (ceq or clr);
   cry <= adder(12) = '1';
   
   -- adder logic
   with nr.es select
   adder <= std_logic_vector(unsigned("0" & ea) +                  1)   when es_inc,
            std_logic_vector(unsigned("0" & ea) -                  1)   when es_dec,
            std_logic_vector(unsigned("0" & ea) + unsigned("0" & eb))   when es_add,
            std_logic_vector(unsigned("0" & ea) - unsigned("0" & eb))   when others;

   -- execution unit election
   with nr.es select
   ex <= ea(10 downto 0) & '0'      when es_lsl,
         ea(11) & ea(11 downto 1)   when es_lsr,
         '0'    & ea(11 downto 1)   when es_asr,
         ea(10 downto 0) & ea(11)   when es_rol,
         ea( 0) & ea(11 downto 1)   when es_ror,
         adder(11 downto 0)         when es_inc|
                                         es_dec|
                                         es_add|
                                         es_sub,
         ea and eb                  when es_and,
         ea or  eb                  when es_orr,
         ea xor eb                  when es_xor,
                eb                  when others;

   -- skip state
   process (rs, ck)
   begin
      if (rs = '0') then
         nk <= '1';
      elsif (rising_edge(ck)) then
         
         -- skip conditions
         if (   nk = '0' and
            (  (nr.sk = sk_ceq and ceq) or
               (nr.sk = sk_clr and clr) or
               (nr.sk = sk_cgr and cgr) or
               (nr.sk = sk_cry and cry)   )) then
            nk <= '1';
         else
            nk <= '0';
         end if;
      end if;
   end process;
   
end rtl;
