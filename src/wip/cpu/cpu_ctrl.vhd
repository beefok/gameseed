-- 0hh      j        s8
-- 100-03F  gotoi    u18
-- 140-07F  calli    u18
-- 180      rst
-- 181-0BF  irq      i6
-- 1Cy      call     ry
-- 1CF      ret
-- 1Dy      goto     ry
-- 1DF      reti
-- 1Ey      sys      #y
-- 1Fy      mov      bp, ry
-- 1FF      movi     bp, im
-- 2xy      ld       rx, ry
-- 3xy      st       rx, ry
-- 4xy      eq       rx, ry
-- 5xy      lr       rx, ry
-- 6xy      gr       rx, ry
-- 7x<      li       rx, s3
-- 7x8      inc      rx
-- 7x9      dec      rx
-- 7xA      rol      rx
-- 7xB      ror      rx
-- 7xC      lsl      rx
-- 7xD      lsr      rx
-- 7xE      asr      rx
-- 7xF      mov      rx, bp
-- >xy      alu      rx, ry
-- >xF      alui     rx, im
--
-- register file:
-- r0 - r7  stack relative
-- r8 - r13 global relative
-- r15 : base pointer         used for memory read/write, acts as the upper word
--       gggggbbbbbbb         it also includes the global pointer used for global register file addressing
-- r14 : stack pointer        used for local register file addressing
--       ....ssssssss

library ieee;
use ieee.std_logic_1164.all;

library work;
use work.cpu_pkg.all;

entity cpu_ctrl is port
(  ir : in  std_logic_vector( 3 downto 0);
   ck : in  std_logic;
   mi : in  std_logic_vector(11 downto 0);
   md : out std_logic_vector(11 downto 0);
   tr : out std_logic_vector(11 downto 0);
   rs : out std_logic;
   cr : out cr_t;
   nr : out cr_t
);
end cpu_ctrl;

architecture rtl of cpu_ctrl is			 

   signal kr : cr_t;

   signal st : std_logic_vector( 1 downto 0);
   signal mr : std_logic_vector(11 downto 0);
   signal iw : std_logic_vector(11 downto 0);
   signal xr : std_logic_vector(11 downto 0);

   signal op, sx, sy       : std_logic_vector( 3 downto 0);
   signal s0, s1, s2, s3   : boolean;

   signal op_rst,   op_irq,   op_sys            : boolean;
   signal op_stbp,  op_stbpi, op_gtbp           : boolean;
   signal op_goto,  op_call,  op_reti, op_ret   : boolean;
   signal op_gotoi, op_calli                    : boolean;
   signal op_ld,    op_ldi,   op_st,   op_sti   : boolean;
   signal op_jmp,   op_eq,    op_eqi            : boolean;
   signal op_lr,    op_lri,   op_gr,   op_gri   : boolean;
   signal op_li,    op_asr,   op_lsl,  op_lsr   : boolean;
   signal op_rol,   op_ror,   op_inc,  op_dec   : boolean;
   signal op_adc,   op_adci,  op_sbc,  op_sbci  : boolean;
   signal op_add,   op_addi,  op_sub,  op_subi  : boolean;
   signal op_mov,   op_movi,  op_and,  op_andi  : boolean;
   signal op_or,    op_ori,   op_xor,  op_xori  : boolean;

begin

   -- output temporary word
   tr <= xr;

   -- output control word
   cr <= kr;
   
   -- memory data in (output to rest of cpu)
   md <= mr;

   -- instruction word
   iw <= mr when s1 else xr;
   
   -- reset control
   rs <= ir(0);
   
   -- registered systems (memory register, temporary register, state register)
   process (ir, ck)
   begin
      if (ir(0) = '0') then
         mr <= (others => '0');
         xr <= (others => '0');
         st <= (others => '0');
         nr <= (nx_0, ma_ip, sk_nop, es_mov, eb_ry, ri_no, x"0", x"0");
      elsif (rising_edge(ck)) then
      
         -- latch a version of the control word
         nr <= kr;
      
         -- latch memory in data
         if    (s1 and ir(0) = '0') then mr <= x"180";
         elsif (s1 and ir(1) = '0') then mr <= x"181";
         elsif (s1 and ir(2) = '0') then mr <= x"182";
         elsif (s1 and ir(3) = '0') then mr <= x"183";
         else                            mr <= mi;
         end if;
         
         -- latch temporary register (stores latest instruction)
         if (s1) then
            xr <= mr;
         end if;
         
         -- update state with next state
         --case (tk) is
         --   when "00"   => tk <= "01"; st <= "00";
         --   when "01"   => tk <= "10"; st <= "00";
         --   when "10"   => tk <= "11"; st <= "00";
         --   when others =>
            
               -- flush the pipeline initially
               case (kr.nx) is
                  when nx_1   => st <= "01";
                  when nx_2   => st <= "10";
                  when nx_3   => st <= "11";
                  when others => st <= "01";
               end case;
               
         --end case;
         
      end if;
   end process;

   -- instruction elements
   op <= iw(11 downto 8);
   sx <= iw( 7 downto 4);
   sy <= iw( 3 downto 0);

   -- state computation
   s0 <= st = "00";
   s1 <= st = "01";
   s2 <= st = "10";
   s3 <= st = "11";

   -- instruction decoding
   op_jmp   <= op = x"0";                          -- ip := ip + s8; ma = ip
   op_calli <= op = x"1" and ((sx  = x"0"    or    -- ma = ip; ma = tr,mi, rt = ip
                               sx  = x"1"    or
                               sx  = x"2"    or
                               sx  = x"3" ));
   op_gotoi <= op = x"1" and ((sx  = x"4"    or    -- ma = ip; ma = tr,mi
                               sx  = x"5"    or
                               sx  = x"6"    or
                               sx  = x"7" ));
                               
   op_rst   <= op = x"1" and ((sx  = x"8"    and   -- ma = u6
                               sy  = x"0" ));
   op_irq   <= op = x"1" and ((sx  = x"8"    and   -- ma = u6, rt = ip
                               sy /= x"0"  ) or
                              (sx  = x"9"    or
                               sx  = x"A"    or
                               sx  = x"B" ));
   op_call  <= op = x"1" and ((sx  = x"C"    and   -- ma = bp,ry, rt = ip
                               sy /= x"F" ));
   op_ret   <= op = x"1" and ((sx  = x"C"    and   -- ma = rt
                               sy  = x"F" ));
   op_goto  <= op = x"1" and ((sx  = x"D"    and   -- ma = bp,ry
                               sy /= x"F" ));
   op_reti  <= op = x"1" and ((sx  = x"D"    and   -- ma = rt, mask = 0
                               sy  = x"F" ));
   op_sys   <= op = x"1" and ((sx  = x"E" ));      -- ma = ip, sys(sy)
   op_stbp  <= op = x"1" and ((sx  = x"F"    and   -- ma = ip, bp = ry
                               sy /= x"F" ));
   op_stbpi <= op = x"1" and ((sx  = x"F"    and   -- ma = ip; ma = ip, bp = mi
                               sy  = x"F" ));
   op_ld    <= op = x"2" and ((sy /= x"F" ));      -- ma = bp,ry; ma = ip, rx = mi
   op_ldi   <= op = x"2" and ((sy  = x"F" ));      -- ma = ip; ma = bp,mi; ma = ip, rx = mi
   op_st    <= op = x"3" and ((sy /= x"F" ));      -- ma = bp,ry:rx; ma = ip
   op_sti   <= op = x"3" and ((sy  = x"F" ));      -- ma = ip; ma = bp,mi:rx; ma = ip
   op_eq    <= op = x"4" and ((sy /= x"F" ));      -- ma = ip, sk = rx eq ry
   op_eqi   <= op = x"4" and ((sy  = x"F" ));      -- ma = ip; ma = ip, sk = rx eq mi
   op_lr    <= op = x"5" and ((sy /= x"F" ));      -- ma = ip, sk = rx lr ry
   op_lri   <= op = x"5" and ((sy  = x"F" ));      -- ma = ip; ma = ip, sk = rx lr mi
   op_gr    <= op = x"6" and ((sy /= x"F" ));      -- ma = ip, sk = rx gr ry
   op_gri   <= op = x"6" and ((sy  = x"F" ));      -- ma = ip; ma = ip, sk = rx gr mi
   op_li    <= op = x"7" and ((sy  = x"0"    or    -- ma = ip, rx = s3
                               sy  = x"1"    or
                               sy  = x"2"    or
                               sy  = x"3"    or
                               sy  = x"4"    or
                               sy  = x"5"    or
                               sy  = x"6"    or
                               sy  = x"7" ));
   op_inc   <= op = x"7" and ((sy  = x"8" ));      -- ma = ip, rx = rx inc 1
   op_dec   <= op = x"7" and ((sy  = x"9" ));      -- ma = ip, rx = rx dec 1
   op_rol   <= op = x"7" and ((sy  = x"A" ));      -- ma = ip, rx = rx rol 1
   op_ror   <= op = x"7" and ((sy  = x"B" ));      -- ma = ip, rx = rx ror 1
   op_lsl   <= op = x"7" and ((sy  = x"C" ));      -- ma = ip, rx = rx lsl 1
   op_lsr   <= op = x"7" and ((sy  = x"D" ));      -- ma = ip, rx = rx lsr 1
   op_asr   <= op = x"7" and ((sy  = x"E" ));      -- ma = ip, rx = rx asr 1
   op_gtbp  <= op = x"7" and ((sy  = x"F" ));      -- ma = ip, rx = bp
   op_adc   <= op = x"8" and ((sy /= x"F" ));      -- ma = ip, rx = rx adc ry
   op_adci  <= op = x"8" and ((sy  = x"F" ));      -- ma = ip; ma = ip, rx = rx adc mi
   op_sbc   <= op = x"9" and ((sy /= x"F" ));      -- ma = ip, rx = rx sbc ry
   op_sbci  <= op = x"9" and ((sy  = x"F" ));      -- ma = ip; ma = ip, rx = rx sbc mi
   op_add   <= op = x"A" and ((sy /= x"F" ));      -- ma = ip, rx = rx add ry
   op_addi  <= op = x"A" and ((sy  = x"F" ));      -- ma = ip; ma = ip, rx = rx add mi
   op_sub   <= op = x"B" and ((sy /= x"F" ));      -- ma = ip, rx = rx sub ry
   op_subi  <= op = x"B" and ((sy  = x"F" ));      -- ma = ip; ma = ip, rx = rx sub mi
   op_mov   <= op = x"C" and ((sy /= x"F" ));      -- ma = ip, rx = ry
   op_movi  <= op = x"C" and ((sy  = x"F" ));      -- ma = ip; ma = ip, rx = mi
   op_and   <= op = x"D" and ((sy /= x"F" ));      -- ma = ip, rx = rx and ry
   op_andi  <= op = x"D" and ((sy  = x"F" ));      -- ma = ip; ma = ip, rx = rx and mi
   op_or    <= op = x"E" and ((sy /= x"F" ));      -- ma = ip, rx = rx or  ry
   op_ori   <= op = x"E" and ((sy  = x"F" ));      -- ma = ip; ma = ip, rx = rx or  mi
   op_xor   <= op = x"F" and ((sy /= x"F" ));      -- ma = ip, rx = rx xor ry
   op_xori  <= op = x"F" and ((sy  = x"F" ));      -- ma = ip; ma = ip, rx = rx xor mi
   
   kr.sx <= sx;
   kr.sy <= sy;

   -- next state
   kr.nx <= nx_3     when  (s2  and (  op_ldi      or
                                       op_sti   )) else
            nx_2     when  (s1  and (  op_jmp      or
                                       op_stbpi    or
                                       op_gotoi    or
                                       op_calli    or
                                       op_ldi      or
                                       op_sti      or
                                       op_eqi      or
                                       op_lri      or
                                       op_gri      or
                                       op_adci     or
                                       op_sbci     or
                                       op_addi     or
                                       op_subi     or
                                       op_movi     or
                                       op_andi     or
                                       op_ori      or
                                       op_xori  )) else
            nx_1;

   -- memory address, ma
   -- 0iiiiiiiiiiiiiiiiii <ip:rd>
   -- 000000000000xxyyyy0 <00,u6:rd>
   -- 0tttttteeeeeeeeeeee <tr,eb:rd>
   -- bbbbbbbeeeeeeeeeeee <bp,eb:rd>
   -- bbbbbbbeeeeeeeeeeee <bp,eb:wr>
   kr.ma <= ma_bpebr when  (s1  and (  op_ld    )) or    -- ip := ip
                           (s2  and (  op_ldi   )) else
            ma_bpebw when  (s1  and (  op_st    )) or    -- ip := ip
                           (s2  and (  op_sti   )) else
            ma_trebr when  (s2  and (  op_gotoi    or    -- ip := ip +  1
                                       op_calli )) else
            ma_eaebr when  (s1  and (  op_goto     or    -- ip := ip +  1
                                       op_call     or
                                       op_ret      or
                                       op_reti  )) else
            ma_u6    when  (s1  and (  op_rst      or    -- ip := ip +  1
                                       op_irq   )) else
            ma_jumpr when  (s1  and (  op_jmp   )) else  -- ip := ma + s8
            ma_ip;

   -- skip comparison
   kr.sk <= sk_ceq   when  (s1  and (  op_eq    )) or
                           (s2  and (  op_eqi   )) else
            sk_clr   when  (s1  and (  op_lr    )) or
                           (s2  and (  op_lri   )) else
            sk_cgr   when  (s1  and (  op_gr    )) or
                           (s2  and (  op_gri   )) else
            sk_cry   when  (s1  and (  op_adc      or
                                       op_sbc   )) or
                           (s2  and (  op_adci     or
                                       op_sbci  )) else
            sk_nop;
   
   -- execute operation
   kr.es <= es_asr   when  (s1  and (  op_asr   )) else
            es_lsl   when  (s1  and (  op_lsl   )) else
            es_lsr   when  (s1  and (  op_lsr   )) else
            es_rol   when  (s1  and (  op_rol   )) else
            es_ror   when  (s1  and (  op_ror   )) else
            es_inc   when  (s1  and (  op_inc   )) else
            es_dec   when  (s1  and (  op_dec   )) else
            es_add   when  (s1  and (  op_adc      or
                                       op_add   )) or
                           (s2  and (  op_adci     or
                                       op_addi  )) else
            es_sub   when  (s1  and (  op_sbc      or
                                       op_sub   )) or
                           (s2  and (  op_sbci     or
                                       op_subi  )) else
            es_and   when  (s1  and (  op_and   )) or
                           (s2  and (  op_andi  )) else
            es_orr   when  (s1  and (  op_or    )) or
                           (s2  and (  op_ori   )) else
            es_xor   when  (s1  and (  op_xor   )) or
                           (s2  and (  op_xori  )) else
            es_mov;
   
   -- execute b selection
   kr.eb <= eb_mi    when  (s2                     or
                            s3                   ) else
            eb_s3    when  (s1  and (  op_li    )) else
            eb_rt    when  (s1  and (  op_ret      or
                                       op_reti  )) else
            eb_ry;
   
   -- register write in
   kr.ri <= ri_ex    when  (s1  and (  op_asr      or
                                       op_lsl      or
                                       op_lsr      or
                                       op_rol      or
                                       op_ror      or
                                       op_inc      or
                                       op_dec      or
                                       op_adc      or
                                       op_add      or
                                       op_sub      or
                                       op_mov      or
                                       op_and      or
                                       op_or       or
                                       op_xor      or
                                       op_gtbp     or
                                       op_stbp  )) or
                           (s2  and (  op_adci     or
                                       op_sbci     or
                                       op_addi     or
                                       op_subi     or
                                       op_movi     or
                                       op_andi     or
                                       op_ori      or
                                       op_xori     or
                                       op_stbpi    or
                                       op_ld    )) or
                           (s3  and (  op_ldi   )) else
            ri_rt    when  (s1  and (  op_irq      or
                                       op_call  )) or
                           (s2  and (  op_calli )) else
            ri_no;

end rtl;
