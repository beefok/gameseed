library ieee;
use ieee.std_logic_1164.all;
use work.cpu_pkg.all;

entity cpu_reg is port
(  rs, ck     : in  std_logic;
	cq         : in  type_cq;
   si, sx, sy : in  std_logic_vector(2 downto 0);
   di         : in  bits;
   rx, ry     : out bits
);
end cpu_reg;

architecture rtl of cpu_reg is

   signal r0, r1, r2, r3, r4, r5, r6, r7 : bits;

begin

   process (ck)
   begin
   
      if (rising_edge(ck)) then
      
         if (rs = '0') then
			
            r0 <= (others => '0');
            r1 <= (others => '0');
            r2 <= (others => '0');
            r3 <= (others => '0');
            r4 <= (others => '0');
            r5 <= (others => '0');
            r6 <= (others => '0');
            r7 <= (others => '0');
				
         else
         
            if (cq.ri /= ri_no) then
               case (si) is
                  when "000" => r0 <= di;
                  when "001" => r1 <= di;
                  when "010" => r2 <= di;
                  when "011" => r3 <= di;
                  when "100" => r4 <= di;
                  when "101" => r5 <= di;
                  when "110" => r6 <= di;
                  when "111" => r7 <= di;
                  when others => null;
               end case;
            end if;
         
         end if;
      
      end if;
   
   end process;
   
   with sx select rx <=
      r0 when "000",
      r1 when "001",
      r2 when "010",
      r3 when "011",
      r4 when "100",
      r5 when "101",
      r6 when "110",
      r7 when others;

   with sy select ry <=
      r0 when "000",
      r1 when "001",
      r2 when "010",
      r3 when "011",
      r4 when "100",
      r5 when "101",
      r6 when "110",
      r7 when others;

end rtl;