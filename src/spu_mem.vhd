library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
library std;
use std.textio.all;

entity spu_mem is port
(  ck, rs : in  std_logic;
-- spu interface
   mw     : in  std_logic;
   ma, mi : in  bits;
   mo     : out bits;
-- dma controller interface
   pw     : in  std_logic;
   pa     : in  phya;
   pi     : in  bits;
   po     : out bits
);
end spu_mem;

architecture rtl of spu_mem is

	type type_ram is array (0 to 511) of std_logic_vector(11 downto 0);

	function init_rom(filename : string) return type_ram is
      file rom_file   : text open read_mode is filename;
      variable ret    : type_ram;
      variable ln     : line;
   begin
      for i in 0 to 511 loop
			if (endfile(rom_file)) then
				ret(i) := (others => '0');
			else
				readline(rom_file, ln);
				oread(ln, ret(i));
			end if;
      end loop;
        
		return ret;
   end function init_rom;

   signal mem : type_ram := init_rom("spu.bin");

begin

   process (ck)
   begin
      if (rising_edge(ck)) then
         if (rs = '0') then
         
            mo <= (others => '0');
            po <= (others => '0');
      
         else
      
            if (mw = '1') then mem(to_integer(unsigned(ma(8 downto 0)))) <= mi; end if;
            mo <= mem(to_integer(unsigned(ma)));
            
            if (pw = '1') then mem(to_integer(unsigned(pa(8 downto 0)))) <= pi; end if;
            po <= mem(to_integer(unsigned(pa(11 downto 0))));
         
         end if;
      end if;
   end process;

end rtl;