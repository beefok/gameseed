library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;

library std;
use std.textio.all;
use work.cpu_pkg.all;

entity gpu_chr is port
(  ck, rs         : in  std_logic;
   md             : in  std_logic;
-- gpu interface
   mw             : in  std_logic;
   ma             : in  std_logic_vector(10 downto 0);
   mi             : in  std_logic_vector( 6 downto 0);
   mo             : out std_logic_vector( 6 downto 0);
-- dma controller interface
   pw, pr         : in  std_logic;
   pa             : in  phya;
   pi             : in  bits;
   po             : out bits
);
end gpu_chr;

architecture rtl of gpu_chr is

	type type_ram is array (0 to 4095) of std_logic_vector(8 downto 0);

	function init_rom(filename : string) return type_ram is
      file rom_file   : text open read_mode is filename;
      variable ret    : type_ram;
      variable ln     : line;
   begin
      for i in 0 to 4095 loop
			if (endfile(rom_file)) then
				ret(i) := (others => '0');
			else
				readline(rom_file, ln);
				oread(ln, ret(i));
			end if;
      end loop;
        
		return ret;
   end function init_rom;

   signal chr0 : type_ram := init_rom("chr0.bin");
   signal chr1 : type_ram := init_rom("chr1.bin");
   signal chr2 : type_ram := init_rom("chr2.bin");

begin

   process (ck, rs)
      variable ba : std_logic_vector(10 downto 0);
   begin
      if (rising_edge(ck)) then
      
         if (rs = '0') then

            mo <= (others => '0');
            po <= (others => '0');
            
         else
         
            -- gpu controller mode
            if (md = '0') then
               
               if (mw = '1') then
                  chr2(to_integer(unsigned(ma & '1'))) <= mi(11 downto 10);
                  chr1(to_integer(unsigned(ma & '1'))) <= mi( 9 downto  8);
                  chr0(to_integer(unsigned(ma & '1'))) <= mi( 7 downto  6);
                  chr2(to_integer(unsigned(ma & '0'))) <= mi( 5 downto  4);
                  chr1(to_integer(unsigned(ma & '0'))) <= mi( 3 downto  2);
                  chr0(to_integer(unsigned(ma & '0'))) <= mi( 1 downto  0);
               end if;
               mo <= chr2(to_integer(unsigned(ma & '1'))) &
                     chr1(to_integer(unsigned(ma & '1'))) &
                     chr0(to_integer(unsigned(ma & '1'))) &
                     chr2(to_integer(unsigned(ma & '0'))) &
                     chr1(to_integer(unsigned(ma & '0'))) &
                     chr0(to_integer(unsigned(ma & '0'))) ;
            
            -- dma controller mode
            else
            
               if (mw = '1') then
                  chr2(to_integer(unsigned(pa(10 downto 1) & '1'))) <= pi(11 downto 10);
                  chr1(to_integer(unsigned(pa(10 downto 1) & '1'))) <= pi( 9 downto  8);
                  chr0(to_integer(unsigned(pa(10 downto 1) & '1'))) <= pi( 7 downto  6);
                  chr2(to_integer(unsigned(pa(10 downto 1) & '0'))) <= pi( 5 downto  4);
                  chr1(to_integer(unsigned(pa(10 downto 1) & '0'))) <= pi( 3 downto  2);
                  chr0(to_integer(unsigned(pa(10 downto 1) & '0'))) <= pi( 1 downto  0);
               end if;
               po <= chr2(to_integer(unsigned(pa(10 downto 1) & '1'))) &
                     chr1(to_integer(unsigned(pa(10 downto 1) & '1'))) &
                     chr0(to_integer(unsigned(pa(10 downto 1) & '1'))) &
                     chr2(to_integer(unsigned(pa(10 downto 1) & '0'))) &
                     chr1(to_integer(unsigned(pa(10 downto 1) & '0'))) &
                     chr0(to_integer(unsigned(pa(10 downto 1) & '0'))) ;
            
            end if;
            
         end if;

      end if;
   end process;

end rtl;