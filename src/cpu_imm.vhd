library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.cpu_pkg.all;

entity cpu_imm is port
(  cq         : in  type_cq;
	iw         : in  bits;
   im         : out bits
);
end cpu_imm;

architecture rtl of cpu_imm is

begin

	process (cq, iw)
		variable sy : std_logic_vector(2 downto 0);
	begin
		sy := iw(2 downto 0);
	
		if ((cq.es = es_shl) or (cq.es = es_shr) or (cq.es = es_rol) or (cq.es = es_ror)) then
			case (sy) is
				when "000"  => im <= x"001";
				when "001"  => im <= x"002";
				when "010"  => im <= x"003";
				when "011"  => im <= x"004";
				when "100"  => im <= x"005";
				when "101"  => im <= x"006";
				when "110"  => im <= x"007";
				when others => im <= x"008";
			end case;
		elsif (cq.eb = eb_ij) then
			im <= iw(10) & iw(10 downto 0);
		else
			case (sy) is
				when "000"  => im <= x"000";
				when "001"  => im <= x"001";
				when "010"  => im <= x"002";
				when "011"  => im <= x"003";
				when "101"  => im <= x"FFD";
				when "110"  => im <= x"FFE";
				when others => im <= x"FFF";
			end case;
		end if;
	end process;
end rtl;