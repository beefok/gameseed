-- system controller : 
-- you have access to different system level controls such as:
-- system timer
-- interrupt handling/masking/acknowledgement
-- button reading

entity sys_top is port
(  rs, ck : in  std_logic;
-- irq interface
   iq     : in  std_logic_vector(3 downto 0);
-- button interface
   bi     : in  std_logic_vector(7 downto 0);
-- i/o register interface
   mw, mr : in  std_logic;
   mi     : in  bits;
   mo     : out bits;
   ir     : out std_logic
);
end sys_top;

architecture rtl of sys_top is

   signal timer_irq : std_logic;
   signal timer : std_logic_vector(15 downto 0);
   signal ar    : std_logic_vector(2 downto 0);
   
   signal irmr, irsr, butr : std_logic_vector(7 downto 0);

begin

   -- we can send an interrupt to the cpu provided irsr masked with irmr returns true
   ir <= '1' when (irsr and irmr) else '0';

   process (rs, ck)
   begin
   
      if (rising_edge(ck)) then
      
         if (rs = '0') then
         
            ar <= (others => '0');
            mo <= (others => '0');
            timer <= (others => '0');
            timer_irq <= '0';
            
            irmr <= (others => '0');
            irsr <= (others => '0');
            butr <= (others => '0');
         
         else
            -- store the latest interrupt request status
            irsr <= "000" & timer_irq & iq;
         
            -- if you write a word to the system controller, it acts as a command:
            if (mw = '1') then
               case (mi(11 downto 8)) is
               
                  -- set address to interrupt mask register and toggle them to n (1 flips, 0 stays)
                  when "0001" => ar <= "000"; irmr <= irmr xor mi(7 downto 0);
                  -- set address to interrupt mask register and set them to n
                  when "0010" => ar <= "000"; irmr <= mi(7 downto 0);
                  
                  -- set address to interrupt status register and acknowledge them to n (1 flips, 0 stays)
                  when "0011" => ar <= "001"; irsr <= irsr xor mi(7 downto 0);
                  
                  -- set address to timer lo. byte register
                  when "1000" => ar <= "100";
                  -- set address to timer lo. byte register and set it to n
                  when "1001" => ar <= "100"; timer <= timer(23 downto  8) & mi(7 downto 0);
                  -- set address to timer md. byte register
                  when "1010" => ar <= "101";
                  -- set address to timer md. byte register and set it to n
                  when "1011" => ar <= "101"; timer <= timer(23 downto 16) & mi(7 downto 0) & timer(7 downto 0);
                  -- set address to timer hi. byte register
                  when "1100" => ar <= "110";
                  -- set address to timer hi. byte register and set it to n
                  when "1101" => ar <= "110"; timer <= mi(7 downto 0) & timer(15 downto 0);
                  
                  -- set address to button register (and latch n buttons)
                  when "1111" => ar <= "111"; butr <= (butr nand mi(7 downto 0)) or (bi and mi(7 downto 0));
                  
               end case;
            elsif (mr = '1') then
               case (ar) is
                  when "000" => mo <= x"0" & irmr;
                  when "001" => mo <= x"0" & irsr;
                  when "100" => mo <= x"0" & timer( 7 downto  0);
                  when "101" => mo <= x"0" & timer(15 downto  8);
                  when "110" => mo <= x"0" & timer(23 downto 16);
                  when "111" => mo <= x"0" & butr;
               end case;
            end if;
            
            -- timer count down
            if (timer = x"0000") then
               timer_irq <= '1';
            else
               timer <= std_logic_vector(unsigned(timer) - 1);            
            end if;
         
         end if;
      
      end if;
   
   end process;

end rtl;