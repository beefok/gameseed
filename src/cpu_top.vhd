library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.cpu_pkg.all;

entity cpu_top is port
(  rs, ck : in  std_logic;
   mb     : out type_mb;
   ma     : out bits;
   mi     : in  bits;
   mo     : out bits
);
end cpu_top;

architecture rtl of cpu_top is

	component cpu_reg is port
	(  rs, ck     : in  std_logic;
		cq         : in  type_cq;
		si, sx, sy : in  std_logic_vector(2 downto 0);
		di         : in  bits;
		rx, ry     : out bits
	);
	end component;
	
	component cpu_alu is port
	(  cq         : in  type_cq;
		ea, eb     : in  bits;
		sk         : out std_logic;
		ex         : out bits
	);
	end component;
	
	component cpu_ctr is port
	(  st : in  type_st;
		md : in  std_logic;
		sk : in  std_logic;
		iw : in  bits;
		cq : out type_cq
	);
	end component;
	
	component cpu_imm is port
	(  cq         : in  type_cq;
		iw         : in  bits;
		im         : out bits
	);
	end component;
	
	signal sx, sy : std_logic_vector(2 downto 0);
	
	signal rx, ry, di, ex, ea, eb, im, iw, ip, tw, mar : bits;
	
	signal cq : type_cq;
	
	signal sk, sr, md, rt : std_logic;
	
	signal st : type_st;

begin

   mb <= cq.mb;

   mo <= rx;

	u1: cpu_ctr port map ( st => st, md => md, sk => sr, iw => iw, cq => cq );
	
	u2: cpu_reg port map ( rs => rs, ck => ck, cq => cq, si => sx, sx => sx, sy => sy, di => di, rx => rx, ry => ry );
	
	u3: cpu_imm port map ( cq => cq, iw => iw, im => im );
	
	u4: cpu_alu port map ( cq => cq, ea => ea, eb => eb, sk => sk, ex => ex );
	
	sx <= iw(5 downto 3);
	sy <= iw(2 downto 0);
	
	ea <= ip when cq.ea = ea_ip else
			ry when cq.ea = ea_ry else
			rx;
			
	eb <= im when cq.eb = eb_im or
					  cq.eb = eb_ij else
			mi when cq.eb = eb_mi else
			ry;
	
   ma <= mar;
	mar<=                            ip when cq.ma = ma_ip else
			"0000000000" & iw(1 downto 0) when cq.ma = ma_io else
			"000000000"  & iw(2 downto 0) when cq.ma = ma_ir else
			ex;
	
	di <= ip when cq.ri = ri_ip else
			ex;
			
	iw <= mi when st = st_s0 else tw;
	
	process (ck)
	begin
	
		if (rising_edge(ck)) then
		
			if (rs = '0') then
				rt <= '0';
				st <= st_s0;
				sr <= '0';
				md <= '0';
				ip <= (others => '0');
				tw <= (others => '0');
			else
				rt <= '1';
			
				-- update state transition
				if (rt = '1') then
					st <= cq.st;
				else
					st <= st_s0;
				end if;
				
				-- store skip condition
				if ((cq.es = es_seq) or (cq.es = es_slr) or (cq.es = es_sgr)) then
					sr <=  sk;
				else
					sr <= '0';
				end if;
				
				-- update interrupt mode
				if (cq.md = md_s0) then
					md <= '0';
				elsif (cq.md = md_s1) then
					md <= '1';
				else
					md <= md;
				end if;
				
				-- update ip
				if (cq.mb = mb_cr) then
					ip <= std_logic_vector(unsigned(mar) + 1);
				else
					ip <= ip;
				end if;
				
				-- update temporary word storage
				if (st = st_s0) then
					tw <= mi;
				else
					tw <= tw;
				end if;

			end if;
		
		end if;
	
	end process;

end rtl;