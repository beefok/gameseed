library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.cpu_pkg.all;

entity dma_top is port
(  rs, ck : in  std_logic;
-- i/o register interface
   mw     : in  std_logic;
   mi     : in  bits;
   ir     : out std_logic;
-- controller interface
   pr, pw : out std_logic;
   pi     : in  bits;
   po     : out bits;
   pa     : out phya
);
end dma_top;

architecture rtl of dma_top is

   type states is (st_src0, st_src1, st_dst0, st_dst1, st_len0, st_read, st_copy);
   signal state : states;

   signal src, dst : phya;
   signal len      : bits;

begin

   process (rs, ck)
   begin
      
      if (rising_edge(ck)) then
      
         if (rs = '0') then
         
            pa  <= (others => '0');
            po  <= (others => '0');
            pr  <= '0';
            pw  <= '0';
            src <= (others => '0');
            dst <= (others => '0');
            len <= (others => '0');
            ir  <= '0';
         
         else
            pr <= '0';
            pw <= '0';
         
            -- i/o register interface
            case (state) is
               when st_src0 => if (mw = '1') then src <= x"000"           & mi; state <= st_src1; ir <= '0'; end if;
               when st_src1 => if (mw = '1') then src <= src(11 downto 0) & mi; state <= st_dst0; ir <= '0'; end if;
               when st_dst0 => if (mw = '1') then dst <= x"000"           & mi; state <= st_dst1; ir <= '0'; end if;
               when st_dst1 => if (mw = '1') then dst <= dst(11 downto 0) & mi; state <= st_len0; ir <= '0'; end if;
               when st_len0 => if (mw = '1') then len <=                    mi; state <= st_read; ir <= '0'; end if;
               
            -- physical address interface
            
            -- read in one word from source
               when st_read =>
                  pa <= src;
                  pr <= '1';
                  
                  src <= std_logic_vector(unsigned(src) + 1);
                  state <= st_copy;

            -- write one word out to destination and loop if len != 0
               when st_copy =>
                  pa <= dst;
                  po <= pi;
                  pw <= '1';
                  
                  dst <= std_logic_vector(unsigned(dst) + 1);
                  
                  if (len = x"000") then
                     state <= st_src0;
                     ir    <= '1';
                  else
                     state <= st_read;
                     len <= std_logic_vector(unsigned(len) - 1);
                     ir    <= '0';
                  end if;
                  
               when others => null;
            end case;
         
         end if;
      
      end if;
   
   end process;

end rtl;