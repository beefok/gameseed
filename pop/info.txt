the pop processor:

   instruction word :
   
      the instruction word, w, is a 24-bit register, where an individual instruction is 6-bit, however, it can include
      multiples of 6-bit extra as an operand.
      because of this, we describe instruction opcodes as two octal digits, since that describes 6-bit cleanly.
      when an instruction word is loaded, the first instruction opcode to be decoded is specified by the lower 2-bit of
      the program pointer, p.
      having an instruction word that holds multiple instructions allows 
      
      fetching a new instruction is actually an instruction itself -- the next instruction (00).
      when the next instruction is called, a new instruction word is requested from main memory
      the instruction word shift register is then shifted in with this data from the memory bus.
      every time an instruction is executed, the instruction word shifts in next instruction(s).
      eventually all that is left on the instruction word shift register are next instructions

   programmer model :
   
      the basic element of any operation is the stack
      
      the top two elements of the stack are called t (top of stack) and n (next of stack).
      these two elements are stored in registers to speed up the processor.
      implementing these stack elements as registers reduces the propagation delay in the execution unit,
      it also removes extended clocking when having to pop and push the stack multiple times for each instruction.
      
      for instance, when you execute an add instruction, it executes t = t + n, n = stack.
      when you execute a li instruction (load immediate), it executes t = immediate, n = t, stack = n.
      without the t and n registers, we would have to pop the stack twice, execute the add, and push the result on the stack
      
      all operations without an operand typically imply use of the top two stack elements
      
      the stack itself is an internal ram that is not connected to main memory
      
      the cpu also stores condition flags, z (zero), c (carry), n (negative)
      they have their typical meanings, and they are to be used with the conditional jump instructions to control flow
      
      the jump instructions have a 6-bit or 12-bit signed jump range and take up either 2 or 4 instruction slots
      
      subroutine calls are also handled with the stack:
      
      when you want to call a subroutine, first you must push all the function args you require for the subroutine call
      
      example:
            lv       3        # load local variable 3
            lv       5        # load local variable 5
            li       2        # load immediate 2
            calli    subr     # call subroutine, pushes the program pointer to stack
            ....
      subr: enter    3        # we need 3 local variables in this subroutine
            ....              # <do subroutine stuff>
            leave             # cleans local variables up off of stack
            goto              # because the program pointer is now top of stack, this is a return from subroutine
      
      the calli instruction pushes the program pointer, p, to stack and sets the program pointer to the subr address.
      the enter instruction moves the stack pointer, s, down by k amount to act as local variables in the subroutine.
      the enter instruction also pushes the frame pointer, v, to the stack so that we know where the previous frame is.
      
      when you use the lv or sv instructions, you can load stack elements off the stack relative to the frame pointer
      this gives you proper subroutine local scope to allow more complex operations required by languages such as c.
      
      make sure that you have no extraneous values on the stack, because when you call the leave instruction,
      it assumes that the top of stack is the old frame pointer, which will then shift the stack pointer to remove space
      allocated for the local variables.
      
      if you have any values you want to return from the subroutine, you need to place them in one or more function args
      and not in local vars, because these will be erased.
      
      note: when you do an enter or leave instruction, the top two stack values follow with you, ignoring the stack pointer
      
      the goto instruction then gets called after the leave instruction, the top of stack should have the return address,
      so it will go to wherever it left off in the instruction word

   context switch :
   
      because the stack is an internal ram that never touches main memory,
      you must dump the entire contents of the stack to a reserved area of memory for the current context
      you must then read in the stack of the other context