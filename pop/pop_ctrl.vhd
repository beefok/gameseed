library ieee;
use ieee.std_logic_1164.all;

library work;
use work.pop_pkg.all;

entity pop_ctrl is
port (
   irq   : in  std_logic_vector( 7 downto 0);
   flags : in  pop_flags;
   wq    : in  std_logic_vector(23 downto 0);
   ctrl  : out pop_ctrl
);
end pop_ctrl;

architecture rtl of pop_ctrl is

   -- instruction control stages
   signal c0 : pop_ctrl;
   signal c1 : pop_ctrl;
   
   -- instruction word decoding
   signal op : std_logic_vector( 5 downto 0);
   signal oa : std_logic_vector( 2 downto 0);
   signal ob : std_logic_vector( 2 downto 0);
   signal ox : std_logic_vector( 5 downto 0);
   signal oy : std_logic_vector( 5 downto 0);
   signal nx : boolean;
   signal ny : boolean;
   signal ir : boolean;
   
   -- condition flag detection
   signal nv, eq, ne, lr, le, gr, ge                                                         : boolean;
   
   -- instruction decoding
   signal op_next, op_nop, op_enter, op_leave, op_reset, op_irq, op_cfg, op_call, op_calli   : boolean;
   signal op_goto, op_gotoi, op_goeq, op_gone, op_golr, op_gole, op_gogr, op_goge            : boolean;
   signal op_lv_u6, op_lv_u3, op_sv_u6, op_sv_u3                                             : boolean;
   signal op_li_0, op_li_1, op_li_i24, op_li_n1, op_li_s6, op_size_s6, op_lx, op_sx          : boolean;
   signal op_dup, op_drop, op_swap, op_over, op_mask, op_unmask, op_unsigned, op_signed      : boolean;
   signal op_inc_u6, op_dec_u6, op_inc, op_dec, op_shl, op_shr, op_rol, op_ror               : boolean;
   signal op_add, op_adc, op_sub, op_sbc, op_and, op_nor, op_or, op_xor                      : boolean;

begin

   -- interrupt detection: if i flag is set, and irq request is non zero and the current instruction is next
   ir <= irq /= x"00" and flags.i = '1' and wq(5 downto 0) = o"00";
   
   -- triplet a is 0 on irq
   oa <= o"0" when ir else wq( 5 downto  3);
   
   -- triplet b is 4 on irq
   ob <= o"4" when ir else wq( 2 downto  0);
   
   -- operand x is normal wq when not irq
   ox <= wq(11 downto  6)  when not ir       else
   -- otherwise it is a specific interrupt id
         o"01"             when irq(1) = '1' else
         o"02"             when irq(2) = '1' else
         o"03"             when irq(3) = '1' else
         o"04"             when irq(4) = '1' else
         o"05"             when irq(5) = '1' else
         o"06"             when irq(6) = '1' else
         o"07"             when irq(7) = '1' else
         o"00";
   
   -- operand y doesn't really matter
   oy <= wq(17 downto 12);
   
   -- complete the opcode for decoding
   op <= oa & ob;
   
   -- detect if operand x or y are zero ("next")
   nx <= ox = o"00";
   ny <= oy = o"00";
   
   -- condition flag comparison
   nv <=   flags.n /= flags.v;
   eq <=   flags.z = '1';                                            -- un/signed,  z
   ne <=   flags.z = '0';                                            -- un/signed, ~z
   lr <= ( flags.s = '0' and flags.c = '0'                   ) or    --  unsigned, ~c
         ( flags.s = '1' and                           not nv);      --    signed,  n ^  v
   le <= ((flags.s = '0' and flags.c = '0') or  flags.z = '1') or    --  unsigned, ~c |  z
         ((flags.s = '1' and flags.z = '1') or         not nv);      --    signed,  z | (n  ^ v)
   gr <= ( flags.s = '0' and flags.c = '1'  and flags.z = '0') or    --  unsigned,  c & ~z
         ( flags.s = '1' and flags.z = '0'  and            nv);      --    signed,  z & (n ~^ v)
   ge <= ( flags.s = '0' and flags.c = '1'                   ) or    --  unsigned,  c
         ( flags.s = '1' and                               nv);      --    signed   n ~^ v

   -- instruction opcode decoding
   op_next     <= ((op = o"00"));
   op_nop      <= ((op = o"01") or
                  ((op = o"12") and (not eq)) or
                  ((op = o"13") and (not ne)) or
                  ((op = o"14") and (not lr)) or
                  ((op = o"15") and (not le)) or
                  ((op = o"16") and (not gr)) or
                  ((op = o"17") and (not ge)));
   op_enter    <= ((op = o"02"));
   op_leave    <= ((op = o"03"));
   op_reset    <= ((op = o"04") and (ox  = o"00"));
   op_irq      <= ((op = o"04") and (ox /= o"00"));
   op_cfg      <= ((op = o"05"));
   op_call     <= ((op = o"06"));
   op_calli    <= ((op = o"07"));
   op_goto     <= ((op = o"10"));
   op_gotoi    <= ((op = o"11"));
   op_goeq     <= ((op = o"12") and (eq        ));
   op_gone     <= ((op = o"13") and (ne        ));
   op_golr     <= ((op = o"14") and (lr        ));
   op_gole     <= ((op = o"15") and (le        ));
   op_gogr     <= ((op = o"16") and (gr        ));
   op_goge     <= ((op = o"17") and (ge        ));
   op_lv_u6    <= ((oa = o"2" ) and (ob  = o"0"));
   op_lv_u3    <= ((oa = o"2" ) and (ob /= o"0"));
   op_sv_u6    <= ((oa = o"3" ) and (ob  = o"0"));
   op_sv_u3    <= ((oa = o"3" ) and (ob /= o"0"));
   op_li_0     <= ((op = o"40"));
   op_li_1     <= ((op = o"41"));
   op_li_i24   <= ((op = o"42"));
   op_li_n1    <= ((op = o"43"));
   op_li_s6    <= ((op = o"44"));
   op_size_s6  <= ((op = o"45"));
   op_lx       <= ((op = o"46"));
   op_sx       <= ((op = o"47"));
   op_dup      <= ((op = o"50"));
   op_drop     <= ((op = o"51"));
   op_swap     <= ((op = o"52"));
   op_over     <= ((op = o"53"));
   op_mask     <= ((op = o"54"));
   op_unmask   <= ((op = o"55"));
   op_unsigned <= ((op = o"56"));
   op_signed   <= ((op = o"57"));
   op_inc_u6   <= ((op = o"60"));
   op_dec_u6   <= ((op = o"61"));
   op_inc      <= ((op = o"62"));
   op_dec      <= ((op = o"63"));
   op_shl      <= ((op = o"64"));
   op_shr      <= ((op = o"65"));
   op_rol      <= ((op = o"66"));
   op_ror      <= ((op = o"67"));
   op_add      <= ((op = o"70"));
   op_adc      <= ((op = o"71"));
   op_sub      <= ((op = o"72"));
   op_sbc      <= ((op = o"73"));
   op_and      <= ((op = o"74"));
   op_nor      <= ((op = o"75"));
   op_or       <= ((op = o"76"));
   op_xor      <= ((op = o"77"));
   
   -- next of stack register, stage 1
   c0.n <=  n_rs  when  op_reset    else
            n_mr  when  op_lx       else
            n_dr  when  op_leave    or
                        op_goto     or
                        op_goeq     or
                        op_gone     or
                        op_golr     or
                        op_gole     or
                        op_gogr     or
                        op_goge     or
                        op_sv_u6    or
                        op_sv_u3    or
                        op_sx       or
                        op_drop     or
                        op_add      or
                        op_adc      or
                        op_sub      or
                        op_sbc      or
                        op_and      or
                        op_or       or
                        op_nor      or
                        op_xor      else
            n_tq  when  op_enter    or
                        op_irq      or
                        op_calli    or
                        op_lv_u6    or
                        op_lv_u3    or
                        op_li_0     or
                        op_li_1     or
                        op_li_i24   or
                        op_li_n1    or
                        op_li_s6    or
                        op_dup      or
                        op_swap     or
                        op_over     else
            n_nq;
   
   -- next of stack register, stage 2
   c1.n <= c0.n;
   
   -- data stack write, stage 1
   c0.d <=  d_v3  when  op_sv_u3    else
            d_v6  when  op_sv_u6    else
            d_nq  when  op_enter    or
                        op_irq      or
                        op_calli    or
                        op_lv_u6    or
                        op_lv_u3    or
                        op_li_0     or
                        op_li_1     or
                        op_li_i24   or
                        op_li_n1    or
                        op_li_s6    or
                        op_lx       or
                        op_dup      or
                        op_over     else
            d_no;
   
   -- data stack write, stage 2
   c1.d <= c0.d;
   
   -- memory bus, stage 1
   c0.m <=  m_rd  when  op_lx       else
            m_wr  when  op_sx       else
            m_ir  when  op_reset    or
                        op_irq      else
            m_gi  when  op_calli    or
                        op_gotoi    else
            m_go  when  op_call     or
                        op_goto     or
                        op_goeq     or
                        op_gone     or
                        op_golr     or
                        op_gole     or
                        op_gogr     or
                        op_goge     else
            m_nx  when  op_next     or    -- todo: implement next speedup
                        op_li_i24   else
            m_no;
            
   -- memory bus, stage 2
   c1.m <=  m_nx  when  (c0.w = w_w1 and nx) or
                        (c0.w = w_w2 and ny) else
            c0.m;
   
   -- program counter, stage 1
   c0.p <=  p_nx  when  c0.m /= m_rd   or
                        c0.m /= m_wr   or
                        c0.m /= m_no   else
            p_no;
   
   -- program counter, stage 2
   c1.p <=  p_nx  when  c1.w = w_nx    else
            c0.p;
   
   -- instruction word, stage 1
   c0.w <=  w_w2  when  op_enter    or
                        op_cfg      or
                        op_lv_u6    or
                        op_sv_u6    or
                        op_size_s6  or
                        op_inc_u6   or
                        op_dec_u6   else
            w_nx  when  op_next     or
                        op_reset    or
                        op_irq      or
                        op_call     or
                        op_calli    or
                        op_goto     or
                        op_gotoi    or
                        op_goeq     or
                        op_gone     or
                        op_golr     or
                        op_gole     or
                        op_gogr     or
                        op_goge     else
            w_w1;

   -- instruction word, stage 2
   c1.w <=  w_nx  when  (c0.w = w_w1 and nx) or
                        (c0.w = w_w2 and ny) else
            c0.w;
   
   -- execution select, stage 1
   c0.e <=  e_cfg when  op_cfg      or
                        op_mask     or
                        op_unmask   or
                        op_unsigned or
                        op_signed   else
            e_nop when  op_next     or
                        op_nop      or
                        op_lx       or
                        op_sx       or
                        op_dup      else
            e_shl when  op_shl      else
            e_shr when  op_shr      else
            e_rol when  op_rol      else
            e_ror when  op_ror      else
            e_add when  op_inc_u6   or
                        op_inc      or
                        op_add      else
            e_adc when  op_adc      else
            e_sub when  op_dec_u6   or
                        op_dec      or
                        op_sub      else
            e_sbc when  op_sbc      else
            e_and when  op_and      else
            e_nor when  op_nor      else
            e_orr when  op_or       else
            e_xor when  op_xor      else
            e_nop;
   
   -- execution select, stage 2
   c1.e <=  c0.e;
   
   -- execution input b, stage 1
   c0.b <=  b_00  when  op_reset    or
                        op_li_0     else
            b_01  when  op_li_1     or
                        op_inc      or
                        op_dec      else
            b_n1  when  op_li_n1    else
            b_mr  when  op_li_i24   else
            b_vq  when  op_enter    else
            b_pq  when  op_irq      or
                        op_call     or
                        op_calli    else
            b_u6  when  op_cfg      or
                        op_inc_u6   or
                        op_dec_u6   else
            b_v6  when  op_lv_u6    else
            b_v3  when  op_lv_u3    else
            b_s6  when  op_li_s6    else
            b_ci  when  op_mask     else
            b_si  when  op_unmask   else
            b_cs  when  op_unsigned else
            b_ss  when  op_signed   else
            b_nq;
   
   -- execution input b, stage 2
   c1.b <=  c0.b;
   
   -- stack frame, stage 1
   c0.v <=  v_rs  when  op_reset    else
            v_en  when  op_enter    else
            v_ex  when  op_leave    else
            v_sz  when  op_size_s6  else
            v_no;
   
   -- stack frame, stage 2
   c1.v <=  c0.v;
   
   -- output stage 2
   ctrl <= c1;
   
end rtl;
