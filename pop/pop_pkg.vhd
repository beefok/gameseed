library ieee;
use ieee.std_logic_1164.all;

package pop_pkg is

   type cr_n is (n_rs, n_nq, n_tq, n_mr, n_dr);
   type cr_d is (d_no, d_nq, d_v3, d_v6);
   type cr_m is (m_no, m_nx, m_rd, m_wr, m_go, m_gi, m_ir);
   type cr_p is (p_no, p_nx);
   type cr_w is (w_no, w_nx, w_w1, w_w2);
   type cr_e is (e_nop, e_mov, e_cfg, e_wot, e_shl, e_shr, e_rol, e_ror, e_add, e_adc, e_sub, e_sbc, e_and, e_nor, e_orr, e_xor);
   type cr_b is (b_nq, b_vq, b_pq, b_mr, b_u6, b_s6, b_no, b_v6, b_v3, b_00, b_01, b_n1, b_ci, b_si, b_cs, b_ss);
   type cr_v is (v_rs, v_no, v_en, v_ex, v_sz);
   
   subtype pop_word is std_logic_vector(23 downto 0);
   subtype pop_math is std_logic_vector(24 downto 0);
   subtype pop_usgn is unsigned(24 downto 0);
   subtype pop_bit  is std_logic;
   
   type pop_flags is record
      z : pop_bit; -- Zero
      c : pop_bit; -- Carry
      n : pop_bit; -- Negative
      v : pop_bit; -- oVerflow
      s : pop_bit; -- Signed mode
      i : pop_bit; -- Irq mask
   end record;

   type pop_ctrl is record
      n : cr_n;
      d : cr_d;
      m : cr_m;
      p : cr_p;
      w : cr_w;
      e : cr_e;
      b : cr_b;
      v : cr_v;
   end record;

end package;
