function pad (number, length) {
   if (number.length > length) { return number.slice( -length ); }
   while (number.length < length) { number = "0" + number; }
   return number;
}

function signex (num, ext) {
   var a = (1 << (ext-1)) - 1;
   var b = (1 << (ext-1));
   return (num & a) - ((num & b) == 0 ? 0 : b);
}

function Data() {
   var len =   0400000;
   var lim = len - 1;
   var wid = 077777777;
   var mem = new Array(len);
   var p = 000000000, w = 000000000;
   var x = 000000000;
   var i;
   
   function reset ( )         { x = 0; p = 0; w = 0; for (i = 0; i < len; i++) { wr(i,0); }  }
   function setp  (k)         { p = k & lim;                                                 }
   function getp  ( )         { return p & lim;                                              }
   function getw  ( )         { return w & wid;                                              }
   function setw  (k)         { w = k & wid;                                                 }
   function shift (n)         { w = w >> (n*6);                                              }
   function rd    (a)         { return mem[a & lim] & wid;                                   }
   function wr    (a, k)      { mem[a] = k & wid;                                            }
   function org   (k)         { x = k & lim;                                                 }
   function put   (k)         { wr(x++, k);                                                  }
   function opc   (a,b,c,d)   { put(((d & 077) << 18) |
                                    ((c & 077) << 12) |
                                    ((b & 077) <<  6) |
                                    ((a & 077)      )); }
   function next  ( )         { w  = rd(p++);                                                }
   function jump  (a)         { w  = rd(a); p = (a + 1) & lim;                               }
   
   return {
      mem:     mem,
      reset:   reset,
      getp:    getp,
      setp:    setp,
      getw:    getw,
      setw:    setw,
      shift:   shift,
      rd:      rd,
      wr:      wr,
      org:     org,
      put:     put,
      opc:     opc,
      next:    next,
      jump:    jump
   };
}

function Expr() {
   var len =     02000;
   var lim = len-1;
   var mem = new Array(len);
   var t = 000000000, n = 000000000;
   var v =       lim, s =       lim;
   var i;
   
   function reset ( )   { t = 0; n = 0; v = lim; s = lim; for (i = 0; i < len; i++) { wr(i,0); }    }
   function rd    (a)   { return mem[a & lim];                                            }
   function wr    (a,k) { return mem[a & lim] = k;                                        }
   function push  ( )   { wr(s--,n); n = t;                                               }
   function pop   ( )   { t = n; n = rd(++s);                                             }
   function gett  ( )   { return t;                                                       }
   function sett  (k)   { t = k & 077777777;                                              }
   function getn  ( )   { return n;                                                       }
   function setn  (k)   { n = k & 077777777;                                              }
   function gets  ( )   { return s;                                                       }
   function sets  (k)   { s = k & lim;                                                    }
   function getv  ( )   { return v;                                                       }
   function setv  (k)   { v = k & lim;                                                    }
   function dump  ( )   {
      var strm = "";
      
      console.log("= stack dump ============================================================================");
      
      strm = "*$" + pad((s-1).toString(8), 8) + "= $" + pad((t >>> 0).toString(8), 8) + ": s-1, (@t)";
      if (s-1 == v) { strm = strm + ", (@v)"; }
      console.log(strm);
      
      strm = "*$" + pad((s+0).toString(8), 8) + "= $" + pad((n >>> 0).toString(8), 8) + ": s+0, (@n)";
      if (s == v) { strm = strm + ", (@v)"; }
      console.log(strm);
      
      var i = 1;
      while (s+i < len) {
         strm = " $" + pad((s+i).toString(8), 8) + "= $" + pad((rd(s+i) >>> 0).toString(8), 8) + ": s+" + i;
         if ((s+i) == v) { strm = strm + " (@v)"; }
         if ((s+i) >  v) { strm = strm + " (v" + (s+i - v) + ")"; }
         console.log(strm);
         i++;
      }
      console.log("=========================================================================================");
   }
   
   return {
      mem:     mem,
      reset:   reset,
      gett:    gett,
      sett:    sett,
      getn:    getn,
      setn:    setn,
      getv:    getv,
      setv:    setv,
      gets:    gets,
      sets:    sets,
      rd:      rd,
      wr:      wr,
      push:    push,
      pop:     pop,
      dump:    dump
   };
}

function Sim() {
   var expr = Expr();
   var data = Data();
   var w    = 0;
   var name = {name: "", arg: ""};
   var tick = 0;
   var cs = {z: 0, c: 0, n: 0, i: 0};
   
   function ex_zero (ex) {
      return (ex & 077777777) == 0 ? 1 : 0;
   }
   
   function ex_sign (ex) {
      return (ex >> 23) & 1;
   }
   
   function condition (id, cs) {
      switch (id & 7) {
         case 1:  return {n: "jp" , k: cs.n == 1};
         case 2:  return {n: "jeq", k: cs.z == 1};
         case 3:  return {n: "jne", k: cs.z == 0};
         case 4:  return {n: "jlr", k: cs.c == 0};
         case 5:  return {n: "jle", k: cs.c == 0 || cs.z == 1};
         case 6:  return {n: "jgr", k: cs.c == 1 && cs.z == 0};
         case 7:  return {n: "jge", k: cs.c == 1};
      }
      return {n: "j", k: true};;
   }
   
   function reset() {
      name  = {name: "reset", arg: ""};
      cs    = {z: 0, c: 0, n: 0, i: 0};
      
      data.reset();
      expr.reset();
      tick = 0;
   }
   
   function step() {
      var op;
      var tm, tw, tp, tt, tn, ts, tv, ta, tc, tx = 0, ty = 0;
      var ex = 0, sh = 0;
   
      // get register values at current tick
      tp = data.getp();
      tw = data.getw();
      
      tt = expr.gett();
      tt = signex(tt, 24);
      
      tn = expr.getn();
      tn = signex(tn, 24);
      
      ts = expr.gets();
      tv = expr.getv();
      tc = cs;
      
      // text output log of instruction execution
      var txt =    "p: $" + pad(tp.toString(8), 5) +
                 ", w: $" + pad(tw.toString(8), 8) +
                 ", v: $" + pad(tv.toString(8), 4) +
                 ", s: $" + pad(ts.toString(8), 4) +
                 " [z" + tc.z + "c" + tc.c + "n" + tc.n + "i" + tc.i + "]";
      
      //console.log(txt);
      //txt = "";
      
      // make decoding instruction easier
      op      = tw & 0x3F;
      
      // update tick counter
      tick++;
      
      // decode instruction
      switch (op) {
// ############################################################################################################# //
         // next
// ############################################################################################################# //
         case 000:
            name = {name: "next", arg: ""};
            data.next (  );
            break;
// ############################################################################################################# //
         // nop
// ############################################################################################################# //
         case 001:
            name = {name: "nop", arg: ""};
            data.shift( 1);
            break;
// ############################################################################################################# //
         // irq u6         m = k, p = m + 1, t = p, n = t, d = s:n, s = s - 1
// ############################################################################################################# //
         case 002:
            ta = (tw >> 6) & 077;
            name = {name: "irq", arg: ta.toString(8)};
            
            expr.push (  );
            expr.sett (tp);
            data.jump (ta);
            
            break;
// ############################################################################################################# //
         // calli i17      m = k, p = m + 1, t = p, n = t, d = s:n, s = s - 1
         // gotoi i17      m = k, p = m + 1
// ############################################################################################################# //
         case 003:
            ta = (tw >> 6) & 037777;
            
            // is this a calli or gotoi?
            if ((tw >> 23) != 0) {
               expr.push (  );
               expr.sett (tp);
               name = {name: "call", arg: ta.toString(8)};
            } else {
               name = {name: "goto", arg: ta.toString(8)};
            }
            data.jump (ta);
            
            break;
// ############################################################################################################# //
         // call           m = t, p = m + 1, t = p
// ############################################################################################################# //
         case 004:
            name = {name: "call", arg: ""};
            data.jump (tt);
            expr.sett (tp);
            break;
// ############################################################################################################# //
         // goto           d = s+1; m = t, p = m + 1, t = n, n = d, s = s + 1
// ############################################################################################################# //
         case 005:
            name = {name: "goto", arg: ""};
            data.jump (tt);
            expr.pop  (  );
            break;
// ############################################################################################################# //
         // enter u6       t = v, n = t, d = s:n,  s, v = s - k - 1
// ############################################################################################################# //
         case 006:
            ta = (tw >> 6) & 0x3F;
            name = {name: "enter", arg: ta.toString(8)};
            tx = ts - ta - 1;
            
            expr.sett (tv);
            expr.setn (tt);
            expr.wr   (ts, tn)
            expr.sets (tx);
            expr.setv (tx);
            data.shift( 2);
            
            break;
// ############################################################################################################# //
         // leave          d = s+1;    t = n, n = d, s = v + 1, v = t
// ############################################################################################################# //
         case 007:
            name = {name: "leave", arg: ""};
            
            expr.pop  (  );
            expr.setv (tt);
            expr.sets (tv+1);
            data.shift( 1);
            
            break;
            
// ############################################################################################################# //
         // j s6           m = p, p = m + k;    w = 0
// ############################################################################################################# //
         case 010:
         case 011:
         case 012:
         case 013:
         case 014:
         case 015:
         case 016:
         case 017:
            tx = condition(op, cs);
            
            ta = signex(tw >> 6, 6);
            ty = 0;
            
            if (ta == 000) {
               ta = signex(tw >> 12, 12);
               ty = 1;
            }
            
            sh = ta  & 3;
            ta = ta >> 2;
            
            // true condition
            if (tx.k) {
               
               data.setp (tp+ta);
               data.shift( 4);
               data.next (  );   // fake next grab for shifting
               data.shift(sh);   // now shift it by the ammount specified
               
            // false condition
            } else {
               if (ty == 0)   { data.shift( 2); }
               else           { data.shift( 4); }
            }
            
            name = {name: tx.n+"("+tx.k+", sh:" + sh + ")", arg: ta};
            
            break;
// ############################################################################################################# //
         // lv u6
// ############################################################################################################# //
         case 020:
            ta = (tw >> 6) & 077;
            name = {name: "lv.s6", arg: ta};
            
            expr.push (  );
            expr.sett (expr.rd(tv+ta));
            data.shift( 2);
            break;
// ############################################################################################################# //
         // lv u3
// ############################################################################################################# //
         case 021:
         case 022:
         case 023:
         case 024:
         case 025:
         case 026:
         case 027:
            ta = tw & 0007;
            name = {name: "lv.s3", arg: ta};
            
            expr.push (  );
            expr.sett (expr.rd(tv+ta));
            data.shift( 1);
            break;
// ############################################################################################################# //
         // sv u6
// ############################################################################################################# //
         case 030:
            ta = (tw >> 6) & 0077;
            name = {name: "sv.s6", arg: ta};
            
            expr.wr(tv+ta, tt);
            expr.pop  (  );
            data.shift( 2);
            break;
// ############################################################################################################# //
         // sv u3
// ############################################################################################################# //
         case 031:
         case 032:
         case 033:
         case 034:
         case 035:
         case 036:
         case 037:
            ta = tw & 0007;
            name = {name: "sv.s3", arg: ta};
            
            expr.wr(tv+ta, tt);
            expr.pop  (  );
            data.shift( 1);
            break;
// ############################################################################################################# //
         // li 0           t = 0, n = t, d = s:n, s = s - 1
// ############################################################################################################# //
         case 040:
            
            name = {name: "li", arg: 0};
            expr.push (  );
            expr.sett ( 0);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // li 1          t = 1, n = t, d = s:n, s = s - 1
// ############################################################################################################# //
         case 041:
            
            name = {name: "li", arg: 1};
            expr.push (  );
            expr.sett ( 1);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // li i24         m = p, p = m + 1, t = m, n = t, d = s:n, s = s - 1
// ############################################################################################################# //
         case 042:
            tx = data.rd(tp);
            data.setp(tp+1);
            
            name = {name: "li.i24", arg: tx};
            expr.push (  );
            expr.sett (tx);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // li -1          t = -1, n = t, d = s:n, s = s - 1
// ############################################################################################################# //
         case 043:
            
            name = {name: "li", arg: -1};
            expr.push (  );
            expr.sett (-1);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // li s6          t = k, n = t, d = s:n, s = s - 1
// ############################################################################################################# //
         case 044:
            ta = signex(tw >> 6, 6);
            
            name = {name: "li.s6", arg: ta};
            expr.push (  );
            expr.sett (ta);
            data.shift( 2);
            break;
// ############################################################################################################# //
         // li s18         t = k, n = t, d = s:n, s = s - 1
// ############################################################################################################# //
         case 045:
            ta = signex(tw >> 6, 18);
            
            name = {name: "li.s18", arg: ta};
            expr.push (  );
            expr.sett (ta);
            data.shift( 4);
            break;
// ############################################################################################################# //
         // lx             m = t;   n = m, d = s:n, s = s - 1
// ############################################################################################################# //
         case 046:
            tx = data.rd(tt);
            
            name = {name: "lx", arg: ""};
            expr.push (  );
            expr.setn (tx);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // sx             m = t:n, d = s+1;    n = d, s = s + 1
// ############################################################################################################# //
         case 047:
            data.wr(tt, tn);
            
            name = {name: "sx", arg: ""};
            expr.pop  (  );
            expr.sett (tt);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // cmp
// ############################################################################################################# //
         case 050:
            name = {name: "cmp", arg: ""};
            
            ex   = tt - tn;
            
            cs.c  = (ex >> 24) & 1;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.pop  (  );
            data.shift( 1);
            break;
// ############################################################################################################# //
         // cmn
// ############################################################################################################# //
         case 051:
            name = {name: "cmn", arg: ""};
            
            ex   = tt + tn;
            
            cs.c  = (ex >> 24) & 1;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.pop  (  );
            data.shift( 1);
            break;
// ############################################################################################################# //
         // tst
// ############################################################################################################# //
         case 052:
            name = {name: "tst", arg: ""};
            
            ex    = tt & tn;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.pop  (  );
            data.shift( 1);
            break;
// ############################################################################################################# //
         // teq
// ############################################################################################################# //
         case 053:
            name = {name: "teq", arg: ""};
            
            ex    = tt ^ tn;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.pop  (  );
            data.shift( 1);
            break;
// ############################################################################################################# //
         // dup
// ############################################################################################################# //
         case 054:
            name = {name: "dup", arg: ""};
            
            expr.push (  );
            data.shift( 1);
            break;
// ############################################################################################################# //
         // swap
// ############################################################################################################# //
         case 055:
            name = {name: "swap", arg: ""};
            
            expr.sett (tn);
            expr.setn (tt);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // drop
// ############################################################################################################# //
         case 056:
            name = {name: "drop", arg: ""};
            
            expr.pop  (  );
            data.shift( 1);
            break;
// ############################################################################################################# //
         // size s6        s = s + k
// ############################################################################################################# //
         case 057:
            ta = signex(tw >> 6, 6);
            
            name = {name: "size.s6", arg: ta};
            expr.sets (ts+ta);
            data.shift( 2);
            break;
// ############################################################################################################# //
         // inc.s6
// ############################################################################################################# //
         case 060:
            ta = signex(tw >> 6, 6);
            name = {name: "inc", arg: ta};
            
            ex = tt + ta;
            
            cs.c  = (ex >> 24) & 1;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.sett (ex);
            data.shift( 2);
            break;
// ############################################################################################################# //
         // asr
// ############################################################################################################# //
         case 061:
            name = {name: "asr", arg: ""};
            
            ex    = tt >> 1;
            
            cs.c  = tt & 1;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.sett (ex);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // lsl
// ############################################################################################################# //
         case 062:
            name = {name: "lsl", arg: ""};
            
            ex    = (tt << 1);
            
            cs.c  = (tt >> 23) & 1;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.sett (ex);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // lsr
// ############################################################################################################# //
         case 063:
            name = {name: "lsr", arg: ""};
            
            ex    = (tt >>> 1);
            
            cs.c  = (tt >> 23) & 1;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.sett (ex);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // rol
// ############################################################################################################# //
         case 064:
            name = {name: "rol", arg: ""};
            
            ex    = (tt << 1) | cs.c;
            
            cs.c  = (tt >> 23) & 1;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.sett (ex);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // ror
// ############################################################################################################# //
         case 065:
            name = {name: "ror", arg: ""};
            
            ex    = (tt >>> 1) | (cs.c << 23);
            
            cs.c  = (tt & 1);
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.sett (ex);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // inc
// ############################################################################################################# //
         case 066:
            name = {name: "inc", arg: ""};
            
            ex = tt + 1;
            
            cs.c  = (ex >> 24) & 1;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.sett (ex);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // dec
// ############################################################################################################# //
         case 067:
            name = {name: "dec", arg: ""};
            
            ex = tt - 1;
            
            cs.c  = (ex >> 24) & 1;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.sett (ex);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // add
// ############################################################################################################# //
         case 070:
            name = {name: "add", arg: ""};
            
            ex   = tt + tn;
            
            cs.c  = (ex >> 24) & 1;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.pop  (  );
            expr.sett (ex);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // adc
// ############################################################################################################# //
         case 071:
            name = {name: "adc", arg: ""};
            
            ex    = ((tt << 1) | 1) + ((tn << 1) | cs.c);
            ex  >>= 1;
            
            cs.c  = (ex >> 24) & 1;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.pop  (  );
            expr.sett (ex);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // sub
// ############################################################################################################# //
         case 072:
            name = {name: "sub", arg: ""};
            
            ex   = tt - tn;
            
            cs.c  = (ex >> 24) & 1;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.pop  (  );
            expr.sett (ex);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // sbc
// ############################################################################################################# //
         case 060:
            name = {name: "sbc", arg: ""};
            
            ex    = ((tt << 1) | 1) - ((tn << 1) | cs.c);
            ex  >>= 1;
            
            cs.c  = (ex >> 24) & 1;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.pop  (  );
            expr.sett (ex);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // and
// ############################################################################################################# //
         case 074:
            name = {name: "and", arg: ""};
            
            ex    = tt & tn;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.pop  (  );
            expr.sett (ex);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // nor
// ############################################################################################################# //
         case 075:
            name = {name: "nor", arg: ""};
            
            ex    = ~(tt | tn);
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.pop  (  );
            expr.sett (ex);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // or
// ############################################################################################################# //
         case 076:
            name = {name: "or", arg: ""};
            
            ex    = tt | tn;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.pop  (  );
            expr.sett (ex);
            data.shift( 1);
            break;
// ############################################################################################################# //
         // xor
// ############################################################################################################# //
         case 077:
            name = {name: "xor", arg: ""};
            
            ex    = tt ^ tn;
            cs.n  = ex_sign(ex);
            cs.z  = ex_zero(ex);
            
            expr.pop  (  );
            expr.sett (ex);
            data.shift( 1);
            break;
            
// ############################################################################################################# //
      }
      
      
      
      txt =    "p: $" + pad(data.getp().toString(8), 5) +
               ", w: $" + pad(data.getw().toString(8), 8) +
               ", v: $" + pad(expr.getv().toString(8), 4) +
               ", s: $" + pad(expr.gets().toString(8), 4) +
               " [z" + cs.z + "c" + cs.c + "n" + cs.n + "i" + cs.i + "] ticks: " + tick;
               
      //console.log(txt);
      console.log(txt, ": $" + pad((op >>> 0).toString(8), 8) + ": " + name.name);
      if (typeof name.arg !== 'string') { console.log(" $" + pad((name.arg >>> 0).toString(8), 8) ); }
      
      console.log("=<======================================================================================="); 
   }
   
   reset();
   
   return {
      reset: reset,
      step:  step,
      expr:  expr,
      data:  data
   };
}

var sim = Sim();

function run_demo_a(sim) {
   sim.reset();
   
   // input the code words
   sim.data.org(0000000);
   sim.data.opc(040, 041, 042, 043);   // li 0, li 1, li.i24, li -1
   sim.data.put(076543210);            // dw 7654321
   sim.data.opc(070, 070, 070, 070);   // add, add, add, add
   
   sim.step();                         // next
   sim.step();                         // li 0
   sim.expr.dump();
   sim.step();                         // li 1
   sim.expr.dump();
   sim.step();                         // li.i24 7654321
   sim.expr.dump();
   sim.step();                         // li -1
   sim.expr.dump();
   sim.step();                         // add
   sim.expr.dump();
   sim.step();                         // add
   sim.expr.dump();
   sim.step();                         // add
   sim.expr.dump();
   sim.step();                         // add
   sim.expr.dump();
   
}

function run_demo_b(sim) {
   sim.reset();
   
   // input the code words
   sim.data.org(0000000);
   sim.data.opc(044, 001, 044, 002);   // li.s6    1;    li.s6    2
   sim.data.opc(044, 003, 044, 004);   // li.s6    3;    li.s6    4
   sim.data.put(040010003);            // calli    100
   sim.data.opc(045, 004, 000, 000);   // size.s6  +4;   next; next
   sim.data.org(0000100);              // subroutine:
   sim.data.opc(006, 000, 021, 022);   // enter  0, lv 1, lv 2
   sim.data.opc(023, 024, 070, 070);   // lv 3, lv 4, add, add
   sim.data.opc(070, 031, 007, 005);   // add, sv 1, leave, goto (return)
   
   // execute
   sim.step();       // next
   sim.expr.dump();
   sim.step();       // li.s6    +1
   sim.expr.dump();
   sim.step();       // li.s6    +2
   sim.expr.dump();
   sim.step();       // next
   sim.step();       // li.s6    +3
   sim.expr.dump();
   sim.step();       // li.s6    +4
   sim.expr.dump();
   sim.step();       // next
   sim.step();       // call     100
   sim.expr.dump();
   
   // ... call ...
   sim.step();       // enter    0
   sim.expr.dump();
   sim.step();       // lv       1
   sim.expr.dump();
   sim.step();       // lv       2
   sim.expr.dump();
   sim.step();       // next
   sim.step();       // lv       3
   sim.expr.dump();
   sim.step();       // lv       4
   sim.expr.dump();
   sim.step();       // add
   sim.expr.dump();
   sim.step();       // add
   sim.expr.dump();
   sim.step();       // next
   sim.step();       // add
   sim.expr.dump();
   sim.step();       // sv       1
   sim.expr.dump();
   sim.step();       // leave
   sim.expr.dump();
   sim.step();       // goto (return)
   sim.expr.dump();
   
   // ... return ...
   sim.step();       // size     +4
   sim.expr.dump();
}

function run_demo_c(sim) {
   sim.reset();
   
   sim.data.org(0000000);
   sim.data.opc(012, 010, 017, 003);   // jeq +10; jge +3
   sim.data.opc(010, 000, 034, 012);   // j 1234
   
   sim.step();       // next
   sim.step();       // jeq +10
   sim.step();       // jge +3
   sim.step();       // next
   sim.step();       // j   +1234
   sim.step();       // next
}

function run_demo_d(sim) {
   sim.reset();
   
   sim.data.org(0000000);
   sim.data.opc(044, 012, 044, 023);   // li 12, li 23
   sim.data.opc(070, 054, 075, 066);   // add, dup, nor, inc
   
   sim.step();       // next
   sim.step();       // li 12
   sim.expr.dump();
   sim.step();       // li 23
   sim.expr.dump();
   sim.step();       // next
   sim.step();       // add
   sim.expr.dump();
   sim.step();       // dup
   sim.expr.dump();
   sim.step();       // nor
   sim.expr.dump();
   sim.step();       // inc
   sim.expr.dump();
   
}