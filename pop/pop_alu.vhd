library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.pop_pkg.all;

entity pop_alu is
port (
   ctrl  : in  pop_ctrl;
   a     : in  pop_word;
   b     : in  pop_word;
   flagi : in  pop_flags;
   flago : out pop_flags;
   q     : out pop_word
);
end pop_alu;

architecture rtl of pop_alu is

   signal cs    : pop_flags;

   signal pa    : pop_bit;
   signal pb    : pop_bit;
   signal pf    : pop_bit;
   signal na    : pop_bit;
   signal nb    : pop_bit;
   signal nf    : pop_bit;
   signal ua    : pop_usgn;
   signal ub    : pop_usgn;

   signal muxer : pop_math;
   signal noper : pop_math;
   signal mover : pop_math;
   signal shler : pop_math;
   signal shrer : pop_math;
   signal roler : pop_math;
   signal rorer : pop_math;
   signal adder : pop_math;
   signal adcer : pop_math;
   signal suber : pop_math;
   signal sbcer : pop_math;
   signal ander : pop_math;
   signal norer : pop_math;
   signal orrer : pop_math;
   signal xorer : pop_math;

begin
   -- todo: implement cfg mode

   -- simplify terms
   pa    <= a(23);               -- msb of a
   na    <= not pa;              -- inversion of msb of a
   pb    <= b(23);               -- msb of b
   nb    <= not pb;              -- inversion of msb of b
   pf    <= cs.n;                -- msb of result
   nf    <= not pf;              -- inversion of msb of result
   ua    <= unsigned('0' & a):   -- unsigned a
   ub    <= unsigned('0' & b):   -- unsigned b
   
   -- interrupt mask flag
   cs.i  <= flagi.i;
   
   -- signed/unsigned math flag
   cs.s  <= flagi.s;
   
   -- zero flag
   cs.z  <= '1'       when    muxer(23 downto 0) = o"00000000" else '0';
   
   -- carry flag
   cs.c  <= muxer(24) when    ctrl.e = e_shl    or
                              ctrl.e = e_shr    or
                              ctrl.e = e_rol    or
                              ctrl.e = e_ror    or
                              ctrl.e = e_add    or
                              ctrl.e = e_adc    or
                              ctrl.e = e_sub    or
                              ctrl.e = e_sbc    else
            flagi.c;
            
   -- negative flag
   cs.n  <= muxer(23);
   
   -- overflow flag
   cs.v  <= (pa and pb and nf) or (na and nb and pf) when ctrl.e = e_add or ctrl.e = e_adc else
            (pa and nb and nf) or (na and pb and pf) when ctrl.e = e_sub or ctrl.e = e_sbc else
            flagi.v;

   -- simple
   noper <=  '0'  & a;
   mover <=  '0'  & b;
   
   -- shifting
   shler <= a(23) & a(22 downto 0) & '0';                                     -- <c,q> = <    a,     0>
   shrer <= a( 0) & a(23)   & a(23 downto 1)    when  flagi.s = '1'  else     -- <q,c> = <a(23),     a>
            a( 0) & '0'     & a(23 downto 1);                                 -- <q,c> = <    0,     a>
   roler <= a(23) & a(22 downto 0) & flagi.c    when  flagi.s = '1'  else     -- <c,q> = <    a,     c>
            a(23) & a(22 downto 0) & a(23);                                   -- <c,q> = <    a, a(23)>
   rorer <= a( 0) & cs.c & a(23 downto 1)       when  flagi.s = '1'  else     -- <q,c> = <    c,     a>
            a( 0) & a(0)    & a(23 downto 1);                                 -- <q,c> = < a(0),     a>
   
   -- logic
   ander <=  '0'  & (a and b);
   norer <=  '0'  & (a nor b);
   orrer <=  '0'  & (a or  b);
   xorer <=  '0'  & (a xor b);
   
   -- arithmetic
   adder <= std_logic_vector(ua + ub + 0);
   adcer <= std_logic_vector(ua + ub + 1) when flagi.c = '1' else
            std_logic_vector(ua + ub + 0);
   suber <= std_logic_vector(ua - ub - 0);
   sbcer <= std_logic_vector(ua - ub - 1) when flagi.c = '1' else
            std_logic_vector(ua - ub - 0);

   -- result mux
   muxer <= mover    when  ctrl.e = e_mov    else
            shler    when  ctrl.e = e_shl    else
            shrer    when  ctrl.e = e_shr    else
            roler    when  ctrl.e = e_rol    else
            rorer    when  ctrl.e = e_ror    else
            adder    when  ctrl.e = e_add    else
            adcer    when  ctrl.e = e_adc    else
            suber    when  ctrl.e = e_sub    else
            sbcer    when  ctrl.e = e_sbc    else
            ander    when  ctrl.e = e_and    else
            norer    when  ctrl.e = e_nor    else
            orrer    when  ctrl.e = e_orr    else
            xorer    when  ctrl.e = e_xor    else
            noper;

            
   flago <= flagi when ctrl.e = e_mov or ctrl.e = e_nop or ctrl.e = e_cfg else
            cs;
            
   q     <= muxer(23 downto 0);
   
end rtl;
